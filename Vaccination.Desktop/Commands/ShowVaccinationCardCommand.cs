﻿namespace Vaccination.Desktop.Commands
{
    using System.Linq;

    using Vaccination.Desktop.Mappers;
    using Vaccination.Desktop.Utils;
    using Vaccination.Desktop.ViewModels;
    using Vaccination.Desktop.Views;

    public class ShowVaccinationCardCommand : BaseCommand
    {
        public override void Execute(object parameter)
        {
            var mainWindow = (MainWindowVm) parameter;
            var selectedAnimalVm = mainWindow.SelectedAnimal;

            var completedCalls = QueryManager.GetCompetedCallsByAnimalId(selectedAnimalVm.Id, out var message);
            if (!message.IsSuccess())
            {
                MessageBoxer.Info(message.Data);
                return;
            }

            var vaccinationCardWindowVm = new VaccinationCardWindowVm
            {
                Animal = selectedAnimalVm,
                Calls = completedCalls.Select(CallMapper.MapCallDtoToCallVm).ToList()
            };

            var vaccinationCardWindow = new VaccinationCardWindow { DataContext = vaccinationCardWindowVm };
            vaccinationCardWindow.Show();
        }
    }
}