﻿namespace Vaccination.Desktop.Commands
{
    using System;

    using Vaccination.Desktop.ViewModels;
    using Vaccination.Desktop.Views;

    public class SignInCommand : BaseCommand
    {
        public override void Execute(object parameter)
        {
            var signInWindowVm = new SignInWindowVm();
            var signInWindow = new SignInWindow { DataContext = signInWindowVm };
            if (signInWindow.ShowDialog() != true)
                Environment.Exit(0);
        }
    }
}