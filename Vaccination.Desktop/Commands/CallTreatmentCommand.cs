﻿namespace Vaccination.Desktop.Commands
{
    using Vaccination.Desktop.Mappers;
    using Vaccination.Desktop.Utils;
    using Vaccination.Desktop.ViewModels;
    using Vaccination.Desktop.Views;

    public class CallTreatmentCommand : BaseCommand
    {
        public override void Execute(object parameter)
        {
            var treatments = QueryManager.GetAllTreatments(out var treatmentMessage);
            if (!treatmentMessage.IsSuccess())
            {
                MessageBoxer.Info(treatmentMessage.Data);
                return;
            }

            var mainWindowVm = (MainWindowVm) parameter;
            var callVaccineVm = new CallTreatmentWindowVm
            {
                Treatments = treatments,
                Animal = mainWindowVm.SelectedAnimal
            };

            var callTreatmentWindow = new CallTreatmentWindow { DataContext = callVaccineVm };
            if (callTreatmentWindow.ShowDialog() != true)
                return;

            var callDto = CallMapper.MapCallTreatmentWindowVmToCallDto(callVaccineVm);
            var callCreateMessage = QueryManager.CreateTreatmentCall(callDto);
            if (!callCreateMessage.IsSuccess())
                MessageBoxer.Info(callCreateMessage.Data);
        }
    }
}