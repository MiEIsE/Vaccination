﻿namespace Vaccination.Desktop.Commands
{
    using Vaccination.Desktop.Utils;
    using Vaccination.Desktop.ViewModels;

    public class CompleteCallCommand : BaseCommand
    {
        public override void Execute(object parameter)
        {
            var mainWindowVm = (MainWindowVm) parameter;
            var selectedCallVm = mainWindowVm.SelectedCall;

            var response = QueryManager.CompleteCall(selectedCallVm.Id);
            if (!response.IsSuccess())
            {
                MessageBoxer.Info(response.Data);
                return;
            }

            mainWindowVm.Update();
        }
    }
}