﻿namespace Vaccination.Desktop.Commands
{
    using System;

    using Vaccination.Desktop.ViewModels;
    using Vaccination.Desktop.Views;

    public class SignOutCommand : BaseCommand
    {
        public override void Execute(object parameter)
        {
            var mainWindow = (MainWindow) parameter;
            mainWindow.Hide();

            var signInWindowVm = new SignInWindowVm();
            var signInWindow = new SignInWindow { DataContext = signInWindowVm };
            if (signInWindow.ShowDialog() != true)
                Environment.Exit(0);

            mainWindow.Show();
            var mainWindowVm = (MainWindowVm) mainWindow.DataContext;
            mainWindowVm.SelectedTabIndex = 0;
            mainWindowVm.Update();
        }
    }
}