﻿namespace Vaccination.Desktop.Commands
{
    using System.Linq;

    using Vaccination.Desktop.Mappers;
    using Vaccination.Desktop.Utils;
    using Vaccination.Desktop.ViewModels;
    using Vaccination.Desktop.Views;

    public class CallVaccineCommand : BaseCommand
    {
        public override void Execute(object parameter)
        {
            var mainWindowVm = (MainWindowVm)parameter;
            var selectedAnimalVm = mainWindowVm.SelectedAnimal;

            var vaccines = QueryManager.GetVaccinesByBreedId(selectedAnimalVm.BreedId, out var vaccinesMessage)
                .Select(GraphicMapper.MapGraphicDtoToVaccineVm)
                .ToList();

            if (!vaccinesMessage.IsSuccess())
            {
                MessageBoxer.Info(vaccinesMessage.Data);
                return;
            }
            
            var callVaccineVm = new CallVaccineWindowVm
            {
                Vaccines = vaccines,
                VaccineId = vaccines.FirstOrDefault()?.Id ?? 0,
                Animal = selectedAnimalVm
            };

            var callVaccineWindow = new CallVaccineWindow { DataContext = callVaccineVm };
            if (callVaccineWindow.ShowDialog() != true)
                return;

            var callDto = CallMapper.MapCallVaccineWindowVmToCallDto(callVaccineVm);
            var callCreateMessage = QueryManager.CreateVaccineCall(callDto);
            if (!callCreateMessage.IsSuccess())
                MessageBoxer.Info(callCreateMessage.Data);
        }
    }
}