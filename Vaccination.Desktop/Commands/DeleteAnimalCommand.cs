﻿namespace Vaccination.Desktop.Commands
{
    using Vaccination.Desktop.Utils;
    using Vaccination.Desktop.ViewModels;

    public class DeleteAnimalCommand : BaseCommand
    {
        public override void Execute(object parameter)
        {
            var mainWindowVm = (MainWindowVm) parameter;
            var selectedAnimalVm = mainWindowVm.SelectedAnimal;
            var response = QueryManager.DeleteAnimal(selectedAnimalVm.Id);
            if (!response.IsSuccess())
            {
                MessageBoxer.Info(response.Data);
                return;
            }

            mainWindowVm.Update();
        }
    }
}