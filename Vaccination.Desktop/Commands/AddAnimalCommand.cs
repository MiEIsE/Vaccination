﻿namespace Vaccination.Desktop.Commands
{
    using Vaccination.Desktop.Mappers;
    using Vaccination.Desktop.Utils;
    using Vaccination.Desktop.ViewModels;
    using Vaccination.Desktop.Views;

    public class AddAnimalCommand : BaseCommand
    {
        public override void Execute(object parameter)
        {
            var breeds = QueryManager.GetBreeds(out var message);
            if (!message.IsSuccess())
            {
                MessageBoxer.Info(message.Data);
                return;
            }

            var animalWindowVm = new AnimalWindowVm { Breeds = breeds };
            var animalWindow = new AnimalWindow { DataContext = animalWindowVm };
            if (animalWindow.ShowDialog() != true)
                return;

            var animalDto = AnimalMapper.MapAnimalWindowVmToAnimalDto(animalWindowVm);
            var createResult = QueryManager.CreateAnimal(animalDto);
            if (!createResult.IsSuccess())
            {
                MessageBoxer.Info(message.Data);
                return;
            }

            var mainWindowVm = (MainWindowVm) parameter;
            mainWindowVm.Update();
        }
    }
}