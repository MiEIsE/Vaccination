﻿namespace Vaccination.Desktop.Commands
{
    using Vaccination.Desktop.Mappers;
    using Vaccination.Desktop.Utils;
    using Vaccination.Desktop.ViewModels;
    using Vaccination.Desktop.Views;

    public class EditAnimalCommand : BaseCommand
    {
        public override void Execute(object parameter)
        {
            var breeds = QueryManager.GetBreeds(out var message);
            if (!message.IsSuccess())
            {
                MessageBoxer.Info(message.Data);
                return;
            }

            var mainWindowVm = (MainWindowVm) parameter;
            var selectedAnimal = mainWindowVm.SelectedAnimal;
            var animalWindowVm = AnimalMapper.MapAnimalVmToWindowVm(selectedAnimal);
            animalWindowVm.Breeds = breeds;

            var animalWindow = new AnimalWindow { DataContext = animalWindowVm };
            if (animalWindow.ShowDialog() != true)
                return;

            var animalDto = AnimalMapper.MapAnimalWindowVmToAnimalDto(animalWindowVm);
            var createResult = QueryManager.EditAnimal(animalDto);
            if (!createResult.IsSuccess())
            {
                MessageBoxer.Info(message.Data);
                return;
            }

            mainWindowVm.Update();
        }
    }
}