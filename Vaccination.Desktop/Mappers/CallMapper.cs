﻿namespace Vaccination.Desktop.Mappers
{
    using Vaccination.DataModel.DTO;
    using Vaccination.Desktop.ViewModels;

    public static class CallMapper
    {
        public static CallVm MapCallDtoToCallVm(CallDto callDto)
        {
            return new CallVm
            {
                Address = callDto.Address,
                BreedName = callDto.BreedName,
                Date = callDto.Date,
                Id = callDto.Id,
                WorkName = callDto.WorkName,
                WorkType = callDto.WorkType
            };
        }

        public static CallDto MapCallTreatmentWindowVmToCallDto(CallTreatmentWindowVm callTreatmentWindowVm)
        {
            return new CallDto
            {
                Address = callTreatmentWindowVm.Address,
                AnimalId = callTreatmentWindowVm.Animal.Id,
                Date = callTreatmentWindowVm.TreatmentDate,
                WorkId = callTreatmentWindowVm.TreatmentId
            };
        }

        public static CallDto MapCallVaccineWindowVmToCallDto(CallVaccineWindowVm callVaccineWindowVm)
        {
            return new CallDto
            {
                Address = callVaccineWindowVm.Address,
                AnimalId = callVaccineWindowVm.Animal.Id,
                Date = callVaccineWindowVm.VaccineDate,
                WorkId = callVaccineWindowVm.VaccineId
            };
        }
    }
}