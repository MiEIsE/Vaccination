﻿namespace Vaccination.Desktop.Mappers
{
    using Vaccination.DataModel.DTO;
    using Vaccination.Desktop.Utils;
    using Vaccination.Desktop.ViewModels;

    public static class AnimalMapper
    {
        public static AnimalVm MapAnimalDtoToVm(AnimalDto animalDto)
        {
            return new AnimalVm
            {
                Birthday = animalDto.Birthday,
                BreedId = animalDto.BreedId,
                BreedName = animalDto.BreedName,
                Id = animalDto.Id,
                Name = animalDto.Name
            };
        }

        public static AnimalWindowVm MapAnimalVmToWindowVm(AnimalVm animalVm)
        {
            return new AnimalWindowVm
            {
                Birthday = animalVm.Birthday,
                BreedId = animalVm.BreedId,
                Id = animalVm.Id,
                Name = animalVm.Name
            };
        }

        public static AnimalDto MapAnimalWindowVmToAnimalDto(AnimalWindowVm animalWindowVm)
        {
            return new AnimalDto
            {
                Birthday = animalWindowVm.Birthday,
                BreedId = animalWindowVm.BreedId,
                Id = animalWindowVm.Id,
                Name = animalWindowVm.Name,
                UserId = UserInfo.Instance.UserId
            };
        }
    }
}