﻿namespace Vaccination.Desktop.Mappers
{
    using Vaccination.DataModel.DTO;
    using Vaccination.Desktop.ViewModels;

    public static class GraphicMapper
    {
        public static VaccineVm MapGraphicDtoToVaccineVm(GraphicDto graphicDto)
        {
            return new VaccineVm
            {
                Id = graphicDto.VaccineId,
                Name = graphicDto.VaccineName
            };
        }

        public static GraphicVm MapGraphicDtoToVm(GraphicDto graphicDto)
        {
            return new GraphicVm
            {
                BreedName = graphicDto.BreedName,
                Id = graphicDto.Id,
                Term = graphicDto.Term,
                VaccineName = graphicDto.VaccineName
            };
        }
    }
}