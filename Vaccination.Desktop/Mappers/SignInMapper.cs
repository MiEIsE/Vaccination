﻿namespace Vaccination.Desktop.Mappers
{
    using Vaccination.DataModel.DTO;
    using Vaccination.Desktop.ViewModels;

    public static class SignInMapper
    {
        public static SignInDto MapSignInWindowVmToDto(SignInWindowVm signInWindowVm)
        {
            return new SignInDto
            {
                Password = signInWindowVm.Password,
                UserName = signInWindowVm.Login
            };
        }
    }
}