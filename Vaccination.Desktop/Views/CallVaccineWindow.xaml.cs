﻿namespace Vaccination.Desktop.Views
{
    using System.Windows;

    public partial class CallVaccineWindow
    {
        public CallVaccineWindow()
        {
            InitializeComponent();
        }

        private void CancelDialog(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void DialogOk(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}