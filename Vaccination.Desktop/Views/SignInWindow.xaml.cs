﻿namespace Vaccination.Desktop.Views
{
    using System;
    using System.Windows;

    using Vaccination.Desktop.Mappers;
    using Vaccination.Desktop.Utils;
    using Vaccination.Desktop.ViewModels;

    public partial class SignInWindow
    {
        public SignInWindow()
        {
            InitializeComponent();

            PasswordBox.Password = "QWEqwe123!";
        }

        private void CloseWindow(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void DialogOk(object sender, RoutedEventArgs e)
        {
            var signInDto = SignInMapper.MapSignInWindowVmToDto((SignInWindowVm) DataContext);
            var signInResultDto = QueryManager.SignIn(signInDto, out var message);
            if (!message.IsSuccess())
            {
                MessageBoxer.Info(message.Data);
                return;
            }

            var userInfo = UserInfo.Instance;
            userInfo.Role = signInResultDto.Role;
            userInfo.Token = signInResultDto.Token;
            userInfo.UserId = signInResultDto.Id;

            DialogResult = true;
        }

        private void PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (DataContext is SignInWindowVm signInWindowVm)
                signInWindowVm.Password = PasswordBox.Password;
        }
    }
}