﻿namespace Vaccination.Desktop.Views
{
    using System.Windows;

    public partial class CallTreatmentWindow
    {
        public CallTreatmentWindow()
        {
            InitializeComponent();
        }

        private void CancelDialog(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void DialogOk(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}