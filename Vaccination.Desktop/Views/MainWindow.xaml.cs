﻿namespace Vaccination.Desktop.Views
{
    using Vaccination.Desktop.ViewModels;

    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new MainWindowVm();
        }
    }
}