﻿namespace Vaccination.Desktop
{
    public static class Constants
    {
        private const string BASE_API_ADDRESS = "http://localhost:65036/api/";

        public const string COMPLETE_CALL = BASE_API_ADDRESS + "calls/complete/{0}";

        public const string CREATE_ANIMAL = BASE_API_ADDRESS + "animals/create";

        public const string CREATE_TREATMENT_CALL = BASE_API_ADDRESS + "calls/createTreatment";

        public const string CREATE_VACCINE_CALL = BASE_API_ADDRESS + "calls/createVaccine";

        public const string DELETE_ANIMAL = BASE_API_ADDRESS + "animals/{0}";

        public const string EDIT_ANIMAL = BASE_API_ADDRESS + "animals/edit";

        public const string GET_ALL_ANIMALS = BASE_API_ADDRESS + "animals/all";

        public const string GET_ALL_BREEDS = BASE_API_ADDRESS + "breeds/all";

        public const string GET_ALL_GRAPHICS = BASE_API_ADDRESS + "graphics/all";

        public const string GET_ALL_TREATMENTS = BASE_API_ADDRESS + "treatments/all";

        public const string GET_VACCINES_BY_BREED_ID = BASE_API_ADDRESS + "graphics/breedId/{0}";

        public const string GET_ANIMALS_BY_USER_ID = BASE_API_ADDRESS + "animals/byUserId/{0}";

        public const string GET_COMPLETED_CALLS_BY_ANIMAL_ID = BASE_API_ADDRESS + "calls/completed/{0}";

        public const string GET_NOTIFICATIONS_BY_USER_ID = BASE_API_ADDRESS + "graphics/notifications/{0}";

        public const string GET_UNCOMPLETED_CALLS = BASE_API_ADDRESS + "calls/uncompleted";

        public const string SIGN_IN = BASE_API_ADDRESS + "auth/signIn";
    }
}