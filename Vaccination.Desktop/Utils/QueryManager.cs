﻿namespace Vaccination.Desktop.Utils
{
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;

    using Newtonsoft.Json;

    using Vaccination.DataModel.DTO;
    using Vaccination.Desktop.ViewModels;

    public static class QueryManager
    {
        public static ResponseDto CompleteCall(int callId)
        {
            var url = string.Format(Constants.COMPLETE_CALL, callId);
            return PutQuery(url);
        }

        public static ResponseDto CreateAnimal(AnimalDto animalDto)
        {
            return PostQuery(animalDto, Constants.CREATE_ANIMAL);
        }

        public static ResponseDto CreateTreatmentCall(CallDto callDto)
        {
            return PostQuery(callDto, Constants.CREATE_TREATMENT_CALL);
        }

        public static ResponseDto CreateVaccineCall(CallDto callDto)
        {
            return PostQuery(callDto, Constants.CREATE_VACCINE_CALL);
        }

        public static ResponseDto DeleteAnimal(int animalId)
        {
            var url = string.Format(Constants.DELETE_ANIMAL, animalId);
            return DeleteQuery(url);
        }

        public static ResponseDto EditAnimal(AnimalDto animalDto)
        {
            return PutQuery(animalDto, Constants.EDIT_ANIMAL);
        }

        public static List<AnimalDto> GetAllAnimals(out ResponseDto message)
        {
            return GetQuery<List<AnimalDto>>(Constants.GET_ALL_ANIMALS, out message);
        }

        public static List<GraphicDto> GetAllGraphics(out ResponseDto message)
        {
            return GetQuery<List<GraphicDto>>(Constants.GET_ALL_GRAPHICS, out message);
        }

        public static List<TreatmentVm> GetAllTreatments(out ResponseDto message)
        {
            return GetQuery<List<TreatmentVm>>(Constants.GET_ALL_TREATMENTS, out message);
        }

        public static List<GraphicDto> GetVaccinesByBreedId(int breedId, out ResponseDto message)
        {
            var url = string.Format(Constants.GET_VACCINES_BY_BREED_ID, breedId);
            return GetQuery<List<GraphicDto>>(url, out message);
        }

        public static List<AnimalDto> GetAnimalsByUserId(string userId, out ResponseDto message)
        {
            var url = string.Format(Constants.GET_ANIMALS_BY_USER_ID, userId);
            return GetQuery<List<AnimalDto>>(url, out message);
        }

        public static List<BreedVm> GetBreeds(out ResponseDto message)
        {
            return GetQuery<List<BreedVm>>(Constants.GET_ALL_BREEDS, out message);
        }

        public static List<CallDto> GetCompetedCallsByAnimalId(int animalId, out ResponseDto message)
        {
            var url = string.Format(Constants.GET_COMPLETED_CALLS_BY_ANIMAL_ID, animalId);
            return GetQuery<List<CallDto>>(url, out message);
        }

        public static List<NotificationVm> GetNotificationsByUserId(string userId, out ResponseDto message)
        {
            var url = string.Format(Constants.GET_NOTIFICATIONS_BY_USER_ID, userId);
            return GetQuery<List<NotificationVm>>(url, out message);
        }

        public static List<CallDto> GetUncompletedCalls(out ResponseDto message)
        {
            return GetQuery<List<CallDto>>(Constants.GET_UNCOMPLETED_CALLS, out message);
        }

        public static SignInResultDto SignIn(SignInDto signInDto, out ResponseDto message)
        {
            return PostQuery<SignInDto, SignInResultDto>(signInDto, Constants.SIGN_IN, out message);
        }

        private static ResponseDto DeleteQuery(string url)
        {
            var client = GetHttpClient();
            var response = client.DeleteAsync(url).Result;
            var stringResult = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<ResponseDto>(stringResult);
        }

        private static HttpClient GetHttpClient()
        {
            return new HttpClient
            {
                DefaultRequestHeaders =
                {
                    Authorization = new AuthenticationHeaderValue("Bearer", UserInfo.Instance.Token)
                }
            };
        }

        private static StringContent GetJsonContent(string jsonString)
        {
            return new StringContent(jsonString, Encoding.UTF8, "application/json");
        }

        private static T GetQuery<T>(string url, out ResponseDto message)
        {
            var client = GetHttpClient();
            var response = client.GetAsync(url).Result;
            var stringResult = response.Content.ReadAsStringAsync().Result;
            if (!response.IsSuccessStatusCode)
            {
                message = JsonConvert.DeserializeObject<ResponseDto>(stringResult);
                return default(T);
            }

            message = new ResponseDto();
            return JsonConvert.DeserializeObject<ResponseDto<T>>(stringResult).Data;
        }

        private static TO PostQuery<TI, TO>(TI data, string url, out ResponseDto message)
        {
            var jsonString = JsonConvert.SerializeObject(data);
            var client = GetHttpClient();
            var response = client.PostAsync(url, GetJsonContent(jsonString)).Result;
            var stringResult = response.Content.ReadAsStringAsync().Result;
            if (!response.IsSuccessStatusCode)
            {
                message = JsonConvert.DeserializeObject<ResponseDto>(stringResult);
                return default(TO);
            }

            message = new ResponseDto();
            return JsonConvert.DeserializeObject<ResponseDto<TO>>(stringResult).Data;
        }

        private static ResponseDto PostQuery<T>(T data, string url)
        {
            var jsonString = JsonConvert.SerializeObject(data);
            var client = GetHttpClient();
            var response = client.PostAsync(url, GetJsonContent(jsonString)).Result;
            var stringResult = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<ResponseDto>(stringResult);
        }

        private static ResponseDto PutQuery<T>(T data, string url)
        {
            var jsonString = JsonConvert.SerializeObject(data);
            var client = GetHttpClient();
            var response = client.PutAsync(url, GetJsonContent(jsonString)).Result;
            var stringResult = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<ResponseDto>(stringResult);
        }

        private static ResponseDto PutQuery(string url)
        {
            var client = GetHttpClient();
            var response = client.PutAsync(url, null).Result;
            var stringResult = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<ResponseDto>(stringResult);
        }
    }
}