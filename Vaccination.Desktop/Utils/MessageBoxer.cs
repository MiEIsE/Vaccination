﻿namespace Vaccination.Desktop.Utils
{
    using System.Windows;

    public static class MessageBoxer
    {
        public static void Info(string text)
        {
            MessageBox.Show(text, "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}