﻿namespace Vaccination.Desktop.Utils
{
    using Vaccination.DataModel.DTO;

    public static class Extensions
    {
        public static bool IsSuccess(this ResponseDto responseDto)
        {
            return responseDto.Data == "OK";
        }
    }
}