﻿namespace Vaccination.Desktop.Utils
{
    public class UserInfo
    {
        static UserInfo()
        {
            Instance = new UserInfo();
        }

        private UserInfo()
        {
            Clean();
        }

        public static UserInfo Instance { get; }

        public string Role { get; set; }

        public string Token { get; set; }

        public string UserId { get; set; }

        public void Clean()
        {
            Role = string.Empty;
            Token = string.Empty;
            UserId = string.Empty;
        }
    }
}