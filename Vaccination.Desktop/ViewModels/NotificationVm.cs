﻿namespace Vaccination.Desktop.ViewModels
{
    using System;

    public class NotificationVm
    {
        public string AnimalName { get; set; }

        public string BreedName { get; set; }

        public DateTime VaccineDate { get; set; }

        public string VaccineName { get; set; }
    }
}