﻿namespace Vaccination.Desktop.ViewModels
{
    using System;
    using System.Collections.Generic;

    public class CallVaccineWindowVm : BaseVm
    {
        private string _address;

        public CallVaccineWindowVm()
        {
            VaccineDate = DateTime.Today;
        }

        public string Address
        {
            get => _address;
            set
            {
                _address = value;
                OnPropertyChanged(nameof(IsValid));
            }
        }

        public AnimalVm Animal { get; set; }

        public bool IsValid => !string.IsNullOrWhiteSpace(Address);

        public DateTime VaccineDate { get; set; }

        public int VaccineId { get; set; }

        public List<VaccineVm> Vaccines { get; set; }
    }
}