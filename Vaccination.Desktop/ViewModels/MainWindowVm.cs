﻿namespace Vaccination.Desktop.ViewModels
{
    using System.Collections.Generic;
    using System.Linq;

    using Vaccination.DataModel;
    using Vaccination.Desktop.Commands;
    using Vaccination.Desktop.Mappers;
    using Vaccination.Desktop.Utils;

    public class MainWindowVm : BaseVm
    {
        private List<AnimalVm> _animals;

        private List<CallVm> _calls;

        private List<GraphicVm> _graphics;

        private List<NotificationVm> _notifications;

        private int _selectedTabIndex;

        public MainWindowVm()
        {
            SignInCommand = new SignInCommand();
            SignOutCommand = new SignOutCommand();
            AddAnimalCommand = new AddAnimalCommand();
            EditAnimalCommand = new EditAnimalCommand();
            DeleteAnimalCommand = new DeleteAnimalCommand();
            CallVaccineCommand = new CallVaccineCommand();
            CallTreatmentCommand = new CallTreatmentCommand();
            CompleteCallCommand = new CompleteCallCommand();
            ShowVaccinationCardCommand = new ShowVaccinationCardCommand();
            SignInCommand.Execute(this);

            Update();
        }

        public AddAnimalCommand AddAnimalCommand { get; }

        public List<AnimalVm> Animals
        {
            get
            {
                var animalsDto = UserInfo.Instance.Role == Constants.USER_ROLE
                    ? QueryManager.GetAnimalsByUserId(UserInfo.Instance.UserId, out var _)
                    : QueryManager.GetAllAnimals(out var _);

                var animals = animalsDto?.Select(AnimalMapper.MapAnimalDtoToVm).ToList();
                _animals = animals ?? _animals;
                return _animals;
            }
        }

        public List<CallVm> Calls
        {
            get
            {
                if (UserInfo.Instance.Role == Constants.USER_ROLE)
                    return _calls;

                var calls = QueryManager.GetUncompletedCalls(out var message)
                    ?.Select(CallMapper.MapCallDtoToCallVm)
                    .ToList();

                if (!message.IsSuccess())
                    MessageBoxer.Info(message.Data);

                _calls = calls ?? _calls;
                return _calls;
            }
        }

        public CallTreatmentCommand CallTreatmentCommand { get; }

        public CallVaccineCommand CallVaccineCommand { get; }

        public CompleteCallCommand CompleteCallCommand { get; }

        public DeleteAnimalCommand DeleteAnimalCommand { get; }

        public EditAnimalCommand EditAnimalCommand { get; }

        public List<GraphicVm> Graphics
        {
            get
            {
                var graphics = QueryManager.GetAllGraphics(out var message)
                    ?.Select(GraphicMapper.MapGraphicDtoToVm)
                    .ToList();

                if (!message.IsSuccess())
                    MessageBoxer.Info(message.Data);

                _graphics = graphics ?? _graphics;
                return _graphics;
            }
        }

        public bool IsVisibleAddingAnimal => UserInfo.Instance.Role == Constants.USER_ROLE;

        public bool IsVisibleCalls => UserInfo.Instance.Role == Constants.VETERINAR_ROLE;

        public bool IsVisibleCallTreatment => UserInfo.Instance.Role == Constants.USER_ROLE;

        public bool IsVisibleCallVaccine => UserInfo.Instance.Role == Constants.USER_ROLE;

        public bool IsVisibleDeletingAnimal => UserInfo.Instance.Role == Constants.USER_ROLE;

        public bool IsVisibleEditingAnimal => UserInfo.Instance.Role == Constants.USER_ROLE;

        public bool IsVisibleNotifications => UserInfo.Instance.Role == Constants.USER_ROLE;

        public List<NotificationVm> Notifications
        {
            get
            {
                if (UserInfo.Instance.Role == Constants.VETERINAR_ROLE)
                    return _notifications;

                var notifications = QueryManager.GetNotificationsByUserId(UserInfo.Instance.UserId, out var message);

                if (!message.IsSuccess())
                    MessageBoxer.Info(message.Data);

                _notifications = notifications ?? _notifications;
                return _notifications;
            }
        }

        public AnimalVm SelectedAnimal { get; set; }

        public CallVm SelectedCall { get; set; }

        public int SelectedTabIndex
        {
            get => _selectedTabIndex;
            set
            {
                _selectedTabIndex = value;
                OnPropertyChanged();
            }
        }

        public ShowVaccinationCardCommand ShowVaccinationCardCommand { get; }

        public SignInCommand SignInCommand { get; }

        public SignOutCommand SignOutCommand { get; }

        public void Update()
        {
            OnPropertyChanged(nameof(Animals));
            OnPropertyChanged(nameof(Calls));
            OnPropertyChanged(nameof(Graphics));
            OnPropertyChanged(nameof(Notifications));
            OnPropertyChanged(nameof(IsVisibleCalls));
            OnPropertyChanged(nameof(IsVisibleNotifications));
            OnPropertyChanged(nameof(IsVisibleAddingAnimal));
            OnPropertyChanged(nameof(IsVisibleCallTreatment));
            OnPropertyChanged(nameof(IsVisibleCallVaccine));
            OnPropertyChanged(nameof(IsVisibleDeletingAnimal));
            OnPropertyChanged(nameof(IsVisibleEditingAnimal));
        }
    }
}