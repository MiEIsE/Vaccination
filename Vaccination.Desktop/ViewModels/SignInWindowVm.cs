﻿namespace Vaccination.Desktop.ViewModels
{
    public class SignInWindowVm : BaseVm
    {
        private string _login;

        private string _password;

        public SignInWindowVm()
        {
            Login = "user";
            //Login = "veterinar";
            Password = "QWEqwe123!";
            OnPropertyChanged(nameof(IsValid));
        }

        public bool IsValid => !string.IsNullOrWhiteSpace(Login) && !string.IsNullOrWhiteSpace(Password);

        public string Login
        {
            get => _login;
            set
            {
                _login = value;
                OnPropertyChanged(nameof(IsValid));
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                OnPropertyChanged(nameof(IsValid));
            }
        }
    }
}