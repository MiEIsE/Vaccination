﻿namespace Vaccination.Desktop.ViewModels
{
    using System;

    public class CallVm : BaseVm
    {
        public string Address { get; set; }

        public string BreedName { get; set; }

        public DateTime Date { get; set; }

        public int Id { get; set; }

        public string WorkName { get; set; }

        public string WorkType { get; set; }
    }
}