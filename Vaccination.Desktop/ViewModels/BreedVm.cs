﻿namespace Vaccination.Desktop.ViewModels
{
    public class BreedVm : BaseVm
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}