﻿namespace Vaccination.Desktop.ViewModels
{
    using System;

    public class AnimalVm : BaseVm
    {
        public DateTime Birthday { get; set; }

        public int BreedId { get; set; }

        public string BreedName { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }
    }
}