﻿namespace Vaccination.Desktop.ViewModels
{
    using System.Collections.Generic;

    public class VaccinationCardWindowVm : BaseVm
    {
        public AnimalVm Animal { get; set; }

        public List<CallVm> Calls { get; set; }
    }
}