﻿namespace Vaccination.Desktop.ViewModels
{
    public class GraphicVm : BaseVm
    {
        public string BreedName { get; set; }

        public int Id { get; set; }

        public int Term { get; set; }

        public string VaccineName { get; set; }
    }
}