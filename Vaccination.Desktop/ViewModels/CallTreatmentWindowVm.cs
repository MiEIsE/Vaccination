﻿namespace Vaccination.Desktop.ViewModels
{
    using System;
    using System.Collections.Generic;

    public class CallTreatmentWindowVm : BaseVm
    {
        private string _address;

        public CallTreatmentWindowVm()
        {
            TreatmentId = 1;
            TreatmentDate = DateTime.Today;
        }

        public string Address
        {
            get => _address;
            set
            {
                _address = value;
                OnPropertyChanged(nameof(IsValid));
            }
        }

        public AnimalVm Animal { get; set; }

        public bool IsValid => !string.IsNullOrWhiteSpace(Address);

        public int TreatmentId { get; set; }

        public List<TreatmentVm> Treatments { get; set; }

        public DateTime TreatmentDate { get; set; }
    }
}