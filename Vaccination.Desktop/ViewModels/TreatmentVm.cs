﻿namespace Vaccination.Desktop.ViewModels
{
    public class TreatmentVm : BaseVm
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}