﻿namespace Vaccination.Desktop.ViewModels
{
    using System;
    using System.Collections.Generic;

    public class AnimalWindowVm : BaseVm
    {
        private string _name;

        public AnimalWindowVm()
        {
            Breeds = new List<BreedVm>();
            Birthday = DateTime.Today;
            BreedId = 1;
        }

        public DateTime Birthday { get; set; }

        public int BreedId { get; set; }

        public List<BreedVm> Breeds { get; set; }

        public int Id { get; set; }

        public bool IsValid => !string.IsNullOrWhiteSpace(Name);

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged(nameof(IsValid));
            }
        }
    }
}