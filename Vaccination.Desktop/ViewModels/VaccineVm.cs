﻿namespace Vaccination.Desktop.ViewModels
{
    public class VaccineVm
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}