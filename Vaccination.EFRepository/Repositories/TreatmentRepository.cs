﻿namespace Vaccination.EFRepository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Vaccination.DataModel.Interfaces;
    using Vaccination.DataModel.Models;
    using Vaccination.EFRepository.Exceptions;

    public class TreatmentRepository : ITreatmentsRepository
    {
        private const string NULL_TREATMENT = "Лечение имеет значение null.";

        private const string TREATMENT_NAME_MISMATCH =
            "Наименование лечения {0} не соответствует справочному значению {1}.";

        private const string TREATMENT_NAME_NULL_OR_EMPTY =
            "Наименование лечения имеет значение null или является пустой строкой.";

        private const string TREATMENT_NOT_FOUND = "Лечение с идентификатором {0} не найдено.";

        private readonly VaccinationDbContext _dbContext;

        public TreatmentRepository(VaccinationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Treatment> GetAll()
        {
            return _dbContext.Treatments;
        }

        public Treatment GetById(int id)
        {
            var foundTreatment = _dbContext.Treatments.FirstOrDefault(t => t.Id == id);
            if (foundTreatment == null)
                throw new EntityNotFoundException(string.Format(TREATMENT_NOT_FOUND, id));

            return foundTreatment;
        }

        public void Validate(Treatment treatment)
        {
            if (treatment == null)
                throw new ArgumentNullException(NULL_TREATMENT);

            if (string.IsNullOrWhiteSpace(treatment.Name))
                throw new ArgumentException(TREATMENT_NAME_NULL_OR_EMPTY);

            var foundTreatment = _dbContext.Treatments.FirstOrDefault(t => t.Id == treatment.Id);
            if (foundTreatment == null)
                throw new EntityNotFoundException(string.Format(TREATMENT_NOT_FOUND, treatment.Id));

            if (treatment.Name != foundTreatment.Name)
                throw new ArgumentException(string.Format(TREATMENT_NAME_MISMATCH, treatment.Name,
                    foundTreatment.Name));
        }
    }
}