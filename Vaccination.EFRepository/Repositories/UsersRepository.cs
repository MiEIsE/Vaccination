﻿namespace Vaccination.EFRepository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Security.Claims;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Identity;
    using Microsoft.IdentityModel.Tokens;

    using Vaccination.DataModel;
    using Vaccination.DataModel.DTO;
    using Vaccination.DataModel.Interfaces;
    using Vaccination.EFRepository.Exceptions;

    public class UsersRepository : IUsersRepository
    {
        private const string NULL_USER = "Пользователь имеет значение null.";

        private const string PASSWORD_DIGITS = "Пароль должен содержать хотя бы одну цифру.";

        private const string PASSWORD_LOWERCASE_CHARACTER = "Пароль должен содержать хотя бы одну строчную букву.";

        private const string PASSWORD_NOT_MATCH_MESSAGE = "Пароли не совпадают.";

        private const string PASSWORD_UPPERCASE_CHARACTER = "Пароль должен содержать хотя бы одну прописную букву.";

        private const string SHORT_LOGIN = "Логин должен содержать минимум 4 символа.";

        private const string SHORT_PASSWORD = "Пароль должен содержать минимум 6 символов.";

        private const string USER_EXIST = "Пользователь с логином {0} уже существует.";

        private const string USER_NOT_FOUND = "Пользователь с идентификатором {0} не найден.";

        private const string WRONG_PASSWORD_MESSAGE = "Неверный пароль.";

        private readonly SignInManager<IdentityUser> _signInManager;

        private readonly UserManager<IdentityUser> _userManager;

        public UsersRepository(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public void EditUser(EditUserProfileDto registerDto)
        {
            var user = _userManager.Users.FirstOrDefault(u => u.Id == registerDto.UserId);
            if (user == null)
                throw new EntityNotFoundException(string.Format(USER_NOT_FOUND, registerDto.UserId));

            if (registerDto.UserName.Length < 4)
                throw new Exception(SHORT_LOGIN);

            if (registerDto.Password != registerDto.ConfirmPassword)
                throw new Exception(PASSWORD_NOT_MATCH_MESSAGE);

            if (registerDto.Password.Length < 6)
                throw new Exception(SHORT_PASSWORD);

            if (Regex.Matches(registerDto.Password, "[A-Z]").Count == 0)
                throw new Exception(PASSWORD_UPPERCASE_CHARACTER);

            if (Regex.Matches(registerDto.Password, "[a-z]").Count == 0)
                throw new Exception(PASSWORD_LOWERCASE_CHARACTER);

            if (Regex.Matches(registerDto.Password, "[0-9]").Count == 0)
                throw new Exception(PASSWORD_DIGITS);

            user.UserName = registerDto.UserName;
            user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, registerDto.Password);

            _userManager.UpdateAsync(user).Wait();
        }

        public IdentityUser GetUserById(string id)
        {
            var foundUser = _userManager.Users.FirstOrDefault(u => u.Id == id);
            if (foundUser == null)
                throw new EntityNotFoundException(string.Format(USER_NOT_FOUND, id));

            return foundUser;
        }

        public void Register(RegisterDto registerDto)
        {
            var user = _userManager.Users.FirstOrDefault(u => u.UserName == registerDto.UserName);

            if (user != null)
                throw new Exception(string.Format(USER_EXIST, registerDto.UserName));

            if (registerDto.UserName.Length < 4)
                throw new Exception(SHORT_LOGIN);

            if (registerDto.Password != registerDto.ConfirmPassword)
                throw new Exception(PASSWORD_NOT_MATCH_MESSAGE);

            if (registerDto.Password.Length < 6)
                throw new Exception(SHORT_PASSWORD);

            if (Regex.Matches(registerDto.Password, "[A-Z]").Count == 0)
                throw new Exception(PASSWORD_UPPERCASE_CHARACTER);

            if (Regex.Matches(registerDto.Password, "[a-z]").Count == 0)
                throw new Exception(PASSWORD_LOWERCASE_CHARACTER);

            if (Regex.Matches(registerDto.Password, "[0-9]").Count == 0)
                throw new Exception(PASSWORD_DIGITS);

            var newUser = new IdentityUser
            {
                UserName = registerDto.UserName,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            _userManager.CreateAsync(newUser, registerDto.Password).Wait();
            _userManager.AddToRolesAsync(newUser, new[] { Constants.USER_ROLE }).Wait();
        }

        public SignInResultDto SignIn(SignInDto signInDto)
        {
            var user = _signInManager.UserManager.Users.FirstOrDefault(u => u.UserName == signInDto.UserName);

            if (user == null)
                throw new EntityNotFoundException(string.Format(USER_NOT_FOUND, signInDto.UserName));

            if (!_signInManager.UserManager.CheckPasswordAsync(user, signInDto.Password).Result)
                throw new ArgumentException(WRONG_PASSWORD_MESSAGE);

            var signInResultDto = new SignInResultDto
            {
                Id = user.Id,
                Role = _userManager.GetRolesAsync(user).Result.First(),
                Token = GenerateJwtToken(user).Result
            };

            return signInResultDto;
        }

        public void Validate(IdentityUser user)
        {
            if (user == null)
                throw new ArgumentNullException(NULL_USER);

            var foundUser = _userManager.Users.FirstOrDefault(u => u.Id == user.Id);
            if (foundUser == null)
                throw new EntityNotFoundException(string.Format(USER_NOT_FOUND, user.Id));
        }

        private async Task<string> GenerateJwtToken(IdentityUser user)
        {
            if (user == null)
                return null;

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id)
            };

            var roles = await _signInManager.UserManager.GetRolesAsync(user);

            claims.AddRange(roles.Select(r => new Claim(ClaimsIdentity.DefaultRoleClaimType, r)));

            var key = Constants.AuthOptions.GetSymmetricSecurityKey();
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                claims: claims,
                signingCredentials: creds,
                expires: DateTime.Now.AddDays(Constants.AuthOptions.LIFETIMEDAYS)
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}