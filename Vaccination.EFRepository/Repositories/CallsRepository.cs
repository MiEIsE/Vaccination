﻿namespace Vaccination.EFRepository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.EntityFrameworkCore;

    using Vaccination.DataModel;
    using Vaccination.DataModel.Interfaces;
    using Vaccination.DataModel.Models;
    using Vaccination.EFRepository.Exceptions;

    public class CallsRepository : ICallsRepository
    {
        private const string CALL_NOT_FOUND = "Вызов с идентификатором {0} не найден.";

        private const string INVALID_DATE = "Дата вызова врача не может быть до сегодняшнего дня.";

        private const string NULL_OR_EMPTY_ADDRESS = "Адрес пуст или имеет значение null.";

        private readonly IAnimalsRepository _animalsRepository;

        private readonly VaccinationDbContext _dbContext;

        private readonly ITreatmentsRepository _treatmentsRepository;

        private readonly IVaccinesRepository _vaccinesRepository;

        private readonly IWorkTypesRepository _workTypesRepository;

        public CallsRepository(VaccinationDbContext dbContext, IAnimalsRepository animalsRepository,
            ITreatmentsRepository treatmentsRepository, IVaccinesRepository vaccinesRepository,
            IWorkTypesRepository workTypesRepository)
        {
            _dbContext = dbContext;
            _animalsRepository = animalsRepository;
            _treatmentsRepository = treatmentsRepository;
            _vaccinesRepository = vaccinesRepository;
            _workTypesRepository = workTypesRepository;
        }

        public void CompleteCall(int id)
        {
            var foundCall = _dbContext.Calls.FirstOrDefault(c => c.Id == id);
            if (foundCall == null)
                throw new EntityNotFoundException(string.Format(CALL_NOT_FOUND, id));

            foundCall.IsComplete = true;

            _dbContext.Calls.Update(foundCall);
            _dbContext.SaveChanges();
        }

        public void CreateCall(BaseCall call)
        {
            if (call is CallTreatment)
            {
                call.WorkType = _workTypesRepository.GetById(Constants.TREATMENT_WORKTYPE);
            }
            else if (call is CallVaccine)
            {
                call.WorkType = _workTypesRepository.GetById(Constants.VACCINE_WORKTYPE);
            }

            Validate(call);

            _dbContext.Calls.Add(call);
            _dbContext.SaveChanges();
        }

        public IEnumerable<BaseCall> GetCompletedCallsByAnimalId(int animalId)
        {
            var callTreatments = _dbContext.Calls
                .OfType<CallTreatment>()
                .Include(c => c.WorkType)
                .Include(c => c.Treatment)
                .Include(c => c.Animal)
                .ThenInclude(c => c.Breed)
                .Where(c => c.IsComplete && c.Animal.Id == animalId)
                .ToList<BaseCall>();

            var callVaccines = _dbContext.Calls
                .OfType<CallVaccine>()
                .Include(c => c.WorkType)
                .Include(c => c.Vaccine)
                .Include(c => c.Animal)
                .ThenInclude(c => c.Breed)
                .Where(c => c.IsComplete && c.Animal.Id == animalId)
                .ToList<BaseCall>();

            callVaccines.AddRange(callTreatments);
            return callVaccines.AsEnumerable();
        }

        IEnumerable<BaseCall> ICallsRepository.GetUncompletedCalls()
        {
            var callTreatments = _dbContext.Calls
                .OfType<CallTreatment>()
                .Include(c => c.WorkType)
                .Include(c => c.Treatment)
                .Include(c => c.Animal)
                .ThenInclude(c => c.Breed)
                .Where(c => !c.IsComplete)
                .ToList<BaseCall>();

            var callVaccines = _dbContext.Calls
                .OfType<CallVaccine>()
                .Include(c => c.WorkType)
                .Include(c => c.Vaccine)
                .Include(c => c.Animal)
                .ThenInclude(c => c.Breed)
                .Where(c => !c.IsComplete)
                .ToList<BaseCall>();

            callVaccines.AddRange(callTreatments);
            return callVaccines.AsEnumerable();
        }

        public virtual void Validate(BaseCall call)
        {
            if (string.IsNullOrWhiteSpace(call.Address))
                throw new ArgumentException(NULL_OR_EMPTY_ADDRESS);

            _animalsRepository.Validate(call.Animal);

            if (call.Date < DateTime.Today)
                throw new ArgumentException(INVALID_DATE);

            _workTypesRepository.Validate(call.WorkType);

            if (call is CallTreatment callTreatment)
            {
                _treatmentsRepository.Validate(callTreatment.Treatment);
            }
            else if (call is CallVaccine callVaccine)
            {
                _vaccinesRepository.Validate(callVaccine.Vaccine);
            }
        }
    }
}