﻿namespace Vaccination.EFRepository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.EntityFrameworkCore;

    using Vaccination.DataModel.Interfaces;
    using Vaccination.DataModel.Models;
    using Vaccination.EFRepository.Exceptions;

    public class AnimalsRepository : IAnimalsRepository
    {
        private const string ANIMAL_NAME_NULL_OR_EMPTY =
            "Кличка животного имеет значение null или является пустой строкой.";

        private const string ANIMAL_NOT_FOUND = "Животное с идентификатором {0} не найдено.";

        private const string NULL_ANIMAL = "Животное имеет значение null.";

        private const string WRONG_BIRTHDAY = "День рождения {0} не может быть будущим числом.";

        private readonly IBreedsRepository _breedsRepository;

        private readonly IUsersRepository _usersRepository;

        private readonly VaccinationDbContext _dbContext;

        public AnimalsRepository(VaccinationDbContext dbContext,
            IBreedsRepository breedsRepository, IUsersRepository usersRepository)
        {
            _dbContext = dbContext;
            _breedsRepository = breedsRepository;
            _usersRepository = usersRepository;
        }

        public IEnumerable<Animal> GetAnimalsByUserId(string id)
        {
            var user = _usersRepository.GetUserById(id);
            return _dbContext.Animals
                .Include(a => a.Breed)
                .Include(a => a.User)
                .Where(a => a.User.Id == user.Id)
                .OrderBy(a => a.Breed);
        }

        public void Create(Animal animal)
        {
            Validate(animal);

            _dbContext.Animals.Add(animal);
            _dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var foundAnimal = _dbContext.Animals.FirstOrDefault(a => a.Id == id);
            if (foundAnimal == null)
                throw new EntityNotFoundException(string.Format(ANIMAL_NOT_FOUND, id));

            _dbContext.Animals.Remove(foundAnimal);
            _dbContext.SaveChanges();
        }

        public void Edit(Animal animal)
        {
            Validate(animal);

            var foundAnimal = _dbContext.Animals.FirstOrDefault(a => a.Id == animal.Id);
            if (foundAnimal == null)
                throw new EntityNotFoundException(string.Format(ANIMAL_NOT_FOUND, animal.Id));

            foundAnimal.Birthday = animal.Birthday;
            foundAnimal.Breed = animal.Breed;
            foundAnimal.Name = animal.Name;

            _dbContext.Update(foundAnimal);
            _dbContext.SaveChanges();
        }

        public IEnumerable<Animal> GetAll()
        {
            return _dbContext.Animals
                .Include(a => a.Breed)
                .Include(a => a.User)
                .OrderBy(a => a.Breed);
        }

        public Animal GetById(int id)
        {
            var foundAnimal = _dbContext.Animals
                .Include(a => a.Breed)
                .Include(a => a.User)
                .FirstOrDefault(a => a.Id == id);

            if (foundAnimal == null)
                throw new EntityNotFoundException(string.Format(ANIMAL_NOT_FOUND, id));

            return foundAnimal;
        }

        public void Validate(Animal animal)
        {
            if (animal == null)
                throw new ArgumentNullException(NULL_ANIMAL);

            if (animal.Birthday >= DateTime.Today.AddDays(1))
                throw new ArgumentException(string.Format(WRONG_BIRTHDAY, animal.Birthday));

            _breedsRepository.Validate(animal.Breed);

            if (string.IsNullOrWhiteSpace(animal.Name))
                throw new ArgumentException(ANIMAL_NAME_NULL_OR_EMPTY);

            _usersRepository.Validate(animal.User);
        }
    }
}