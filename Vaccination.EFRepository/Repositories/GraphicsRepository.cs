﻿namespace Vaccination.EFRepository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Internal;

    using Vaccination.DataModel.Interfaces;
    using Vaccination.DataModel.Models;
    using Vaccination.EFRepository.Exceptions;

    public class GraphicsRepository : IGraphicsRepository
    {
        private const string GRAPHIC_NOT_FOUND = "График вакцинации с идентификатором {0} не найден.";

        private const string NULL_GRAPHIC = "График вакцинации имеет значение null.";

        private const string WRONG_TERM = "Значение срока проведения вакцинации {0} является некорректным значением.";

        private readonly IBreedsRepository _breedsRepository;

        private readonly VaccinationDbContext _dbContext;

        private readonly IVaccinesRepository _vaccinesRepository;

        public GraphicsRepository(VaccinationDbContext dbContext,
            IBreedsRepository breedsRepository, IVaccinesRepository vaccinesRepository)
        {
            _dbContext = dbContext;
            _breedsRepository = breedsRepository;
            _vaccinesRepository = vaccinesRepository;
        }

        public IEnumerable<Graphic> GetByBreedId(int breedId)
        {
            return _dbContext.Graphics
                .Include(g => g.Breed)
                .Include(g => g.Vaccine)
                .Where(g => g.Breed.Id == breedId)
                .OrderBy(g => g.Breed);
        }


        public IEnumerable<Graphic> GetVaccineByBreedId(int breedId)
        {
            return _dbContext.Graphics
                .Include(g => g.Vaccine)
                .Include(g => g.Breed)
                .Where(g => g.Breed.Id == breedId)
                .OrderBy(g => g.Vaccine)
                .ToList()
                .GroupBy(g => g.Vaccine.Name)
                .Select(g => g.FirstOrDefault())
                .ToList();
        }

        public IEnumerable<Graphic> GetAll()
        {
            return _dbContext.Graphics
                .Include(g => g.Breed)
                .Include(g => g.Vaccine)
                .OrderBy(g => g.Breed);
        }

        public Graphic GetById(int id)
        {
            var foundGraphic = _dbContext.Graphics
                .Include(g => g.Breed)
                .Include(g => g.Vaccine)
                .FirstOrDefault(g => g.Id == id);

            if (foundGraphic == null)
                throw new EntityNotFoundException(string.Format(GRAPHIC_NOT_FOUND, id));

            return foundGraphic;
        }

        public void Validate(Graphic graphic)
        {
            if (graphic == null)
                throw new ArgumentNullException(NULL_GRAPHIC);

            _breedsRepository.Validate(graphic.Breed);

            if (graphic.Term < 0)
                throw new ArgumentException(string.Format(WRONG_TERM, graphic.Term));

            _vaccinesRepository.Validate(graphic.Vaccine);
        }
    }
}