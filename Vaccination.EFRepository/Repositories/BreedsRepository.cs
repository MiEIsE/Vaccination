﻿namespace Vaccination.EFRepository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Vaccination.DataModel.Interfaces;
    using Vaccination.DataModel.Models;
    using Vaccination.EFRepository.Exceptions;

    public class BreedsRepository : IBreedsRepository
    {
        private const string BREED_NAME_MISMATCH =
            "Наименование породы {0} не соответствует справочному значению {1}.";

        private const string BREED_NAME_NULL_OR_EMPTY =
            "Наименование породы имеет значение null или является пустой строкой.";

        private const string BREED_NOT_FOUND = "Порода с идентификатором {0} не найдена.";

        private const string NULL_BREED = "Порода имеет значение null.";

        private readonly VaccinationDbContext _dbContext;

        public BreedsRepository(VaccinationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Breed> GetAll()
        {
            return _dbContext.Breeds;
        }

        public Breed GetById(int id)
        {
            var foundBreed = _dbContext.Breeds.FirstOrDefault(b => b.Id == id);
            if (foundBreed == null)
                throw new EntityNotFoundException(string.Format(BREED_NOT_FOUND, id));

            return foundBreed;
        }

        public void Validate(Breed breed)
        {
            if (breed == null)
                throw new ArgumentNullException(NULL_BREED);

            if (string.IsNullOrWhiteSpace(breed.Name))
                throw new ArgumentException(BREED_NAME_NULL_OR_EMPTY);

            var foundBreed = _dbContext.Breeds.FirstOrDefault(b => b.Id == breed.Id);
            if (foundBreed == null)
                throw new EntityNotFoundException(string.Format(BREED_NOT_FOUND, breed.Id));

            if (breed.Name != foundBreed.Name)
                throw new ArgumentException(string.Format(BREED_NAME_MISMATCH, breed.Name, foundBreed.Name));
        }
    }
}