﻿namespace Vaccination.EFRepository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.EntityFrameworkCore;

    using Vaccination.DataModel.Interfaces;
    using Vaccination.DataModel.Models;
    using Vaccination.EFRepository.Exceptions;

    public class VaccinesRepository : IVaccinesRepository
    {
        private const string NULL_VACCINE = "Вакцина имеет значение null.";

        private const string VACCINE_NAME_MISMATCH =
            "Наименование вакцины {0} не соответствует справочному значению {1}.";

        private const string VACCINE_NAME_NULL_OR_EMPTY =
            "Наименование вакцины имеет значение null или является пустой строкой.";

        private const string VACCINE_NOT_FOUND = "Вакцина с идентификатором {0} не найдена.";

        private readonly VaccinationDbContext _dbContext;

        public VaccinesRepository(VaccinationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Vaccine> GetAll()
        {
            return _dbContext.Vaccines;
        }

        public Vaccine GetById(int id)
        {
            var foundVaccine = _dbContext.Vaccines.FirstOrDefault(v => v.Id == id);
            if (foundVaccine == null)
                throw new EntityNotFoundException(string.Format(VACCINE_NOT_FOUND, id));

            return foundVaccine;
        }

        public void Validate(Vaccine vaccine)
        {
            if (vaccine == null)
                throw new ArgumentNullException(NULL_VACCINE);

            if (string.IsNullOrWhiteSpace(vaccine.Name))
                throw new ArgumentException(VACCINE_NAME_NULL_OR_EMPTY);

            var foundVaccine = _dbContext.Vaccines.FirstOrDefault(t => t.Id == vaccine.Id);
            if (foundVaccine == null)
                throw new EntityNotFoundException(string.Format(VACCINE_NOT_FOUND, vaccine.Id));

            if (vaccine.Name != foundVaccine.Name)
                throw new ArgumentException(string.Format(VACCINE_NAME_MISMATCH, vaccine.Name, foundVaccine.Name));
        }
    }
}