﻿namespace Vaccination.EFRepository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Vaccination.DataModel.Interfaces;
    using Vaccination.DataModel.Models;
    using Vaccination.EFRepository.Exceptions;

    public class WorkTypesRepository : IWorkTypesRepository
    {
        private const string NULL_WORK_TYPE = "Тип работы имеет значение null.";

        private const string WORK_TYPE_NAME_MISMATCH =
            "Наименование типа работы {0} не соответствует справочному значению {1}.";

        private const string WORK_TYPE_NAME_NULL_OR_EMPTY =
            "Наименование типа работы имеет значение null или является пустой строкой.";

        private const string WORK_TYPE_NOT_FOUND = "Тип работы с идентификатором {0} не найден.";

        private readonly VaccinationDbContext _dbContext;

        public WorkTypesRepository(VaccinationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<WorkType> GetAll()
        {
            return _dbContext.WorkTypes;
        }

        public WorkType GetById(int id)
        {
            var foundWorkType = _dbContext.WorkTypes.FirstOrDefault(t => t.Id == id);
            if (foundWorkType == null)
                throw new EntityNotFoundException(string.Format(WORK_TYPE_NOT_FOUND, id));

            return foundWorkType;
        }

        public void Validate(WorkType workType)
        {
            if (workType == null)
                throw new ArgumentNullException(NULL_WORK_TYPE);

            if (string.IsNullOrWhiteSpace(workType.Name))
                throw new ArgumentException(WORK_TYPE_NAME_NULL_OR_EMPTY);

            var foundWorkType = _dbContext.WorkTypes.FirstOrDefault(t => t.Id == workType.Id);
            if (foundWorkType == null)
                throw new EntityNotFoundException(string.Format(WORK_TYPE_NOT_FOUND, workType.Id));

            if (workType.Name != foundWorkType.Name)
                throw new ArgumentException(string.Format(WORK_TYPE_NAME_MISMATCH, workType.Name, foundWorkType.Name));
        }
    }
}