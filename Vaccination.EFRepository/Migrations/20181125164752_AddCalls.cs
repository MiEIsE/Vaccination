﻿namespace Vaccination.EFRepository.Migrations
{
    using Microsoft.EntityFrameworkCore.Metadata;
    using Microsoft.EntityFrameworkCore.Migrations;
    using System;

    public partial class AddCalls : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CallVaccines_Animals_AnimalId",
                table: "CallVaccines");

            migrationBuilder.DropForeignKey(
                name: "FK_CallVaccines_Vaccines_VaccineId",
                table: "CallVaccines");

            migrationBuilder.DropTable(
                name: "CallTreatments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CallVaccines",
                table: "CallVaccines");

            migrationBuilder.RenameTable(
                name: "CallVaccines",
                newName: "Calls");

            migrationBuilder.RenameIndex(
                name: "IX_CallVaccines_VaccineId",
                table: "Calls",
                newName: "IX_Calls_VaccineId");

            migrationBuilder.RenameIndex(
                name: "IX_CallVaccines_AnimalId",
                table: "Calls",
                newName: "IX_Calls_AnimalId");

            migrationBuilder.AlterColumn<int>(
                name: "VaccineId",
                table: "Calls",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Calls",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "TreatmentId",
                table: "Calls",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Calls",
                table: "Calls",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Calls_TreatmentId",
                table: "Calls",
                column: "TreatmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Calls_Animals_AnimalId",
                table: "Calls",
                column: "AnimalId",
                principalTable: "Animals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Calls_Treatments_TreatmentId",
                table: "Calls",
                column: "TreatmentId",
                principalTable: "Treatments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Calls_Vaccines_VaccineId",
                table: "Calls",
                column: "VaccineId",
                principalTable: "Vaccines",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Calls_Animals_AnimalId",
                table: "Calls");

            migrationBuilder.DropForeignKey(
                name: "FK_Calls_Treatments_TreatmentId",
                table: "Calls");

            migrationBuilder.DropForeignKey(
                name: "FK_Calls_Vaccines_VaccineId",
                table: "Calls");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Calls",
                table: "Calls");

            migrationBuilder.DropIndex(
                name: "IX_Calls_TreatmentId",
                table: "Calls");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Calls");

            migrationBuilder.DropColumn(
                name: "TreatmentId",
                table: "Calls");

            migrationBuilder.RenameTable(
                name: "Calls",
                newName: "CallVaccines");

            migrationBuilder.RenameIndex(
                name: "IX_Calls_VaccineId",
                table: "CallVaccines",
                newName: "IX_CallVaccines_VaccineId");

            migrationBuilder.RenameIndex(
                name: "IX_Calls_AnimalId",
                table: "CallVaccines",
                newName: "IX_CallVaccines_AnimalId");

            migrationBuilder.AlterColumn<int>(
                name: "VaccineId",
                table: "CallVaccines",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_CallVaccines",
                table: "CallVaccines",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "CallTreatments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(nullable: false),
                    AnimalId = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    IsComplete = table.Column<bool>(nullable: false),
                    TreatmentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CallTreatments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CallTreatments_Animals_AnimalId",
                        column: x => x.AnimalId,
                        principalTable: "Animals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CallTreatments_Treatments_TreatmentId",
                        column: x => x.TreatmentId,
                        principalTable: "Treatments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CallTreatments_AnimalId",
                table: "CallTreatments",
                column: "AnimalId");

            migrationBuilder.CreateIndex(
                name: "IX_CallTreatments_TreatmentId",
                table: "CallTreatments",
                column: "TreatmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_CallVaccines_Animals_AnimalId",
                table: "CallVaccines",
                column: "AnimalId",
                principalTable: "Animals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CallVaccines_Vaccines_VaccineId",
                table: "CallVaccines",
                column: "VaccineId",
                principalTable: "Vaccines",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}