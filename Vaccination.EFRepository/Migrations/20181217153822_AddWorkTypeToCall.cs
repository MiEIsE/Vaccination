﻿namespace Vaccination.EFRepository.Migrations
{
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class AddWorkTypeToCall : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Calls_Treatments_TreatmentId",
                table: "Calls");

            migrationBuilder.DropForeignKey(
                name: "FK_Calls_Vaccines_VaccineId",
                table: "Calls");

            migrationBuilder.AddColumn<int>(
                name: "WorkTypeId",
                table: "Calls",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Calls_WorkTypeId",
                table: "Calls",
                column: "WorkTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Calls_WorkTypes_WorkTypeId",
                table: "Calls",
                column: "WorkTypeId",
                principalTable: "WorkTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Calls_Treatments_TreatmentId",
                table: "Calls",
                column: "TreatmentId",
                principalTable: "Treatments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Calls_Vaccines_VaccineId",
                table: "Calls",
                column: "VaccineId",
                principalTable: "Vaccines",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Calls_WorkTypes_WorkTypeId",
                table: "Calls");

            migrationBuilder.DropForeignKey(
                name: "FK_Calls_Treatments_TreatmentId",
                table: "Calls");

            migrationBuilder.DropForeignKey(
                name: "FK_Calls_Vaccines_VaccineId",
                table: "Calls");

            migrationBuilder.DropIndex(
                name: "IX_Calls_WorkTypeId",
                table: "Calls");

            migrationBuilder.DropColumn(
                name: "WorkTypeId",
                table: "Calls");

            migrationBuilder.AddForeignKey(
                name: "FK_Calls_Treatments_TreatmentId",
                table: "Calls",
                column: "TreatmentId",
                principalTable: "Treatments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Calls_Vaccines_VaccineId",
                table: "Calls",
                column: "VaccineId",
                principalTable: "Vaccines",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
