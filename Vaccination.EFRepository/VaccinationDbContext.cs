﻿namespace Vaccination.EFRepository
{
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;

    using Vaccination.DataModel.Models;

    public class VaccinationDbContext : IdentityDbContext
    {
        public VaccinationDbContext(DbContextOptions<VaccinationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Animal> Animals { get; set; }

        public DbSet<Breed> Breeds { get; set; }

        public DbSet<BaseCall> Calls { get; set; }

        public DbSet<Graphic> Graphics { get; set; }

        public DbSet<Treatment> Treatments { get; set; }

        public DbSet<Vaccine> Vaccines { get; set; }

        public DbSet<WorkType> WorkTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<CallTreatment>();
            builder.Entity<CallVaccine>();

            base.OnModelCreating(builder);
        }
    }
}