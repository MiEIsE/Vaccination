﻿namespace Vaccination.EFRepository.Utils
{
    using System.Collections.Generic;
    using System.Linq;

    using Vaccination.DataModel.Models;

    public static class CatalogInitializer
    {
        public static void InitializeCatalogs(VaccinationDbContext dbContext)
        {
            InitializeBreeds(dbContext);
            InitializeTreatment(dbContext);
            InitializeVaccine(dbContext);
            InitializeGraphics(dbContext);
            InitializeWorkTypes(dbContext);
        }

        private static void InitializeBreeds(VaccinationDbContext dbContext)
        {
            if (dbContext.Breeds.Any())
                return;

            dbContext.Breeds.AddRange(new List<Breed>
            {
                new Breed
                {
                    Name = "Корова"
                },
                new Breed
                {
                    Name = "Свинья"
                }
            });

            dbContext.SaveChanges();
        }

        private static void InitializeGraphics(VaccinationDbContext dbContext)
        {
            if (dbContext.Graphics.Any())
                return;

            var breeds = dbContext.Breeds.ToList();
            var vaccines = dbContext.Vaccines.ToList();

            dbContext.Graphics.AddRange(new List<Graphic>
            {
                new Graphic
                {
                    Breed = breeds[0],
                    Vaccine = vaccines[0],
                    Term = 30
                },
                new Graphic
                {
                    Breed = breeds[0],
                    Vaccine = vaccines[0],
                    Term = 365 / 2
                },
                new Graphic
                {
                    Breed = breeds[0],
                    Vaccine = vaccines[1],
                    Term = 15
                },
                new Graphic
                {
                    Breed = breeds[0],
                    Vaccine = vaccines[1],
                    Term = 125
                },
                new Graphic
                {
                    Breed = breeds[0],
                    Vaccine = vaccines[2],
                    Term = 15
                },
                new Graphic
                {
                    Breed = breeds[0],
                    Vaccine = vaccines[2],
                    Term = 40
                },
                new Graphic
                {
                    Breed = breeds[1],
                    Vaccine = vaccines[2],
                    Term = 25
                },
                new Graphic
                {
                    Breed = breeds[1],
                    Vaccine = vaccines[3],
                    Term = 25
                },
                new Graphic
                {
                    Breed = breeds[0],
                    Vaccine = vaccines[3],
                    Term = 30
                },
                new Graphic
                {
                    Breed = breeds[0],
                    Vaccine = vaccines[4],
                    Term = 90
                },
                new Graphic
                {
                    Breed = breeds[0],
                    Vaccine = vaccines[5],
                    Term = 90
                },
                new Graphic
                {
                    Breed = breeds[1],
                    Vaccine = vaccines[5],
                    Term = 90
                }
            });

            dbContext.SaveChanges();
        }

        private static void InitializeTreatment(VaccinationDbContext dbContext)
        {
            if (dbContext.Treatments.Any())
                return;

            dbContext.Treatments.AddRange(new List<Treatment>
            {
                new Treatment
                {
                    Name = "Акушерско-гинекологическая помощь"
                },
                new Treatment
                {
                    Name = "Обработка от паразитов"
                },
                new Treatment
                {
                    Name = "Терапевтическая помощь"
                },
                new Treatment
                {
                    Name = "Профилактический осмотр"
                }
            });

            dbContext.SaveChanges();
        }

        private static void InitializeVaccine(VaccinationDbContext dbContext)
        {
            if (dbContext.Vaccines.Any())
                return;

            dbContext.Vaccines.AddRange(new List<Vaccine>
            {
                new Vaccine
                {
                    Name = "Против инфекционного ринотрахеита"
                },
                new Vaccine
                {
                    Name = "Против респираторно-синцитиальной инфекции"
                },
                new Vaccine
                {
                    Name = "Против пастереллеза"
                },
                new Vaccine
                {
                    Name = "Против сальмонеллеза"
                },
                new Vaccine
                {
                    Name = "Против сибирской язвы"
                },
                new Vaccine
                {
                    Name = "Против ящура"
                }
            });

            dbContext.SaveChanges();
        }

        private static void InitializeWorkTypes(VaccinationDbContext dbContext)
        {
            if (dbContext.WorkTypes.Any())
                return;

            dbContext.WorkTypes.AddRange(new List<WorkType>
            {
                new WorkType
                {
                    Name = "Лечение"
                },
                new WorkType
                {
                    Name = "Вакцинация"
                }
            });

            dbContext.SaveChanges();
        }
    }
}