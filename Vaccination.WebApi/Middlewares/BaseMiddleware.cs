﻿namespace Vaccination.WebApi.Middlewares
{
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Http;

    public class BaseMiddleware
    {
        private readonly RequestDelegate _next;

        public BaseMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public virtual async Task Invoke(HttpContext context)
        {
            await _next.Invoke(context);
        }
    }
}