﻿namespace Vaccination.WebApi.Middlewares
{
    using System;
    using System.Net;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Http;

    using Newtonsoft.Json;

    using Vaccination.DataModel.DTO;
    using Vaccination.EFRepository.Exceptions;

    public class HttpStatusCodeExceptionMiddleware : BaseMiddleware
    {
        public HttpStatusCodeExceptionMiddleware(RequestDelegate next) : base(next)
        {
        }

        public override async Task Invoke(HttpContext context)
        {
            try
            {
                await base.Invoke(context);
            }
            catch (ArgumentException ex)
            {
                await WriteResponse(context, ex.Message, HttpStatusCode.BadRequest);
            }
            catch (AccessViolationException ex)
            {
                await WriteResponse(context, ex.Message, HttpStatusCode.Forbidden);
            }
            catch (EntityNotFoundException ex)
            {
                await WriteResponse(context, ex.Message, HttpStatusCode.NotFound);
            }
            catch (AggregateException ex)
            {
                await WriteResponse(context, ex.InnerException.Message);
            }
            catch (Exception ex)
            {
                await WriteResponse(context, ex.Message);
            }
        }

        private static async Task WriteResponse(
            HttpContext context,
            string message,
            HttpStatusCode statusCode = HttpStatusCode.InternalServerError)
        {
            context.Response.StatusCode = (int) statusCode;
            context.Response.ContentType = @"application/json";

            var response = new ResponseDto<string>
            {
                Status = ((int) statusCode).ToString(),
                Data = message
            };

            await context.Response.WriteAsync(JsonConvert.SerializeObject(response));
        }
    }
}