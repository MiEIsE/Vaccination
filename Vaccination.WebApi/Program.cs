﻿namespace Vaccination.WebApi
{
    using System;

    using Microsoft.AspNetCore;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;

    using Vaccination.EFRepository;
    using Vaccination.EFRepository.Utils;
    using Vaccination.WebApi.Utils;

    public class Program
    {
        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
        }

        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                try
                {
                    var dbContext = services.GetRequiredService<VaccinationDbContext>();
                    dbContext.Database.Migrate();
                    CatalogInitializer.InitializeCatalogs(dbContext);

                    var userManager = services.GetService<UserManager<IdentityUser>>();
                    var roleManager = services.GetService<RoleManager<IdentityRole>>();
                    StartupInitializer.Initialize(userManager, roleManager).Wait();
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred while migrating the database.");
                }
            }

            host.Run();
        }
    }
}