﻿namespace Vaccination.WebApi.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    using Vaccination.DataModel;
    using Vaccination.DataModel.DTO;
    using Vaccination.DataModel.Interfaces;
    using Vaccination.DataModel.Models;

    [Route("api/graphics")]
    [Authorize(Roles = Constants.VETERINAR_ROLE + "," + Constants.USER_ROLE)]
    public class GraphicsController : ControllerBase
    {
        private readonly IAnimalsRepository _animalsRepository;

        private readonly IGraphicsRepository _graphicsRepository;

        public GraphicsController(IGraphicsRepository graphicsRepository, IAnimalsRepository animalsRepository)
        {
            _graphicsRepository = graphicsRepository;
            _animalsRepository = animalsRepository;
        }

        [HttpGet("all")]
        public ResponseDto<IEnumerable<GraphicDto>> GetAllGraphics()
        {
            return new ResponseDto<IEnumerable<GraphicDto>>
            {
                Data = _graphicsRepository.GetAll().Select(MapGraphicModelToDto)
            };
        }
        
        [HttpGet("breedId/{breedId}")]
        public ResponseDto<IEnumerable<GraphicDto>> GetVaccineByBreedId(int breedId)
        {
            return new ResponseDto<IEnumerable<GraphicDto>>
            {
                Data = _graphicsRepository.GetVaccineByBreedId(breedId).Distinct().Select(MapGraphicModelToDto)
            };
        }

        [HttpGet("{id}")]
        public ResponseDto<GraphicDto> GetGraphicById(int id)
        {
            return new ResponseDto<GraphicDto>
            {
                Data = MapGraphicModelToDto(_graphicsRepository.GetById(id))
            };
        }

        [HttpGet("notifications/{userId}")]
        [Authorize(Roles = Constants.USER_ROLE)]
        public ResponseDto<IEnumerable<NotificationDto>> GetNotificationsByUserId(string userId)
        {
            var notifications = new List<NotificationDto>();
            var animals = _animalsRepository.GetAnimalsByUserId(userId);
            foreach (var animal in animals)
            {
                var graphics = _graphicsRepository.GetByBreedId(animal.Breed.Id);
                foreach (var graphic in graphics)
                {
                    notifications.Add(new NotificationDto
                    {
                        AnimalName = animal.Name,
                        BreedName = animal.Breed.Name,
                        VaccineDate = animal.Birthday.AddDays(graphic.Term),
                        VaccineName = graphic.Vaccine.Name
                    });
                }
            }

            return new ResponseDto<IEnumerable<NotificationDto>>
            {
                Data = notifications.Where(n => n.VaccineDate >= DateTime.Today && n.VaccineDate <= DateTime.Today.AddMonths(1))
            };
        }

        private static GraphicDto MapGraphicModelToDto(Graphic graphic)
        {
            return new GraphicDto
            {
                BreedId = graphic.Breed?.Id ?? 0,
                BreedName = graphic.Breed?.Name ?? string.Empty,
                Id = graphic.Id,
                Term = graphic.Term,
                VaccineId = graphic.Vaccine?.Id ?? 0,
                VaccineName = graphic.Vaccine?.Name ?? string.Empty
            };
        }
    }
}