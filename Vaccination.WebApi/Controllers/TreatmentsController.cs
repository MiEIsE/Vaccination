﻿namespace Vaccination.WebApi.Controllers
{
    using System.Collections.Generic;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    using Vaccination.DataModel;
    using Vaccination.DataModel.DTO;
    using Vaccination.DataModel.Interfaces;
    using Vaccination.DataModel.Models;

    [Route("api/treatments")]
    [Authorize(Roles = Constants.VETERINAR_ROLE + "," + Constants.USER_ROLE)]
    public class TreatmentsController : ControllerBase
    {
        private readonly ITreatmentsRepository _treatmentsRepository;

        public TreatmentsController(ITreatmentsRepository treatmentsRepository)
        {
            _treatmentsRepository = treatmentsRepository;
        }

        [HttpGet("all")]
        public ResponseDto<IEnumerable<Treatment>> GetAllTreatments()
        {
            return new ResponseDto<IEnumerable<Treatment>>
            {
                Data = _treatmentsRepository.GetAll()
            };
        }

        [HttpGet("{id}")]
        public ResponseDto<Treatment> GetTreatmentById(int id)
        {
            return new ResponseDto<Treatment>
            {
                Data = _treatmentsRepository.GetById(id)
            };
        }
    }
}