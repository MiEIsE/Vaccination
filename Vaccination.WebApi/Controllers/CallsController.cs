﻿namespace Vaccination.WebApi.Controllers
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    using Vaccination.DataModel;
    using Vaccination.DataModel.DTO;
    using Vaccination.DataModel.Interfaces;
    using Vaccination.DataModel.Models;

    [Route("api/calls")]
    public class CallsController : ControllerBase
    {
        private readonly IAnimalsRepository _animalsRepository;

        private readonly ICallsRepository _callsRepository;

        private readonly ITreatmentsRepository _treatmentsRepository;

        private readonly IVaccinesRepository _vaccinesRepository;

        public CallsController(ICallsRepository callsRepository, IAnimalsRepository animalsRepository,
            ITreatmentsRepository treatmentsRepository, IVaccinesRepository vaccinesRepository)
        {
            _callsRepository = callsRepository;
            _animalsRepository = animalsRepository;
            _treatmentsRepository = treatmentsRepository;
            _vaccinesRepository = vaccinesRepository;
        }

        [HttpPut("complete/{id}")]
        [Authorize(Roles = Constants.VETERINAR_ROLE)]
        public ResponseDto CompleteCall(int id)
        {
            _callsRepository.CompleteCall(id);
            return new ResponseDto();
        }

        [HttpPost("createTreatment")]
        [Authorize(Roles = Constants.USER_ROLE)]
        public ResponseDto CreateTreatmentCall([FromBody] CallDto callTreatmentDto)
        {
            _callsRepository.CreateCall(MapCallDtoToCallTreatment(callTreatmentDto));
            return new ResponseDto();
        }

        [HttpPost("createVaccine")]
        [Authorize(Roles = Constants.USER_ROLE)]
        public ResponseDto CreateVaccineCall([FromBody] CallDto callVaccineDto)
        {
            _callsRepository.CreateCall(MapCallDtoToCallVaccine(callVaccineDto));
            return new ResponseDto();
        }

        [HttpGet("completed/{animalId}")]
        [Authorize(Roles = Constants.VETERINAR_ROLE + "," + Constants.USER_ROLE)]
        public ResponseDto<IEnumerable<CallDto>> GetCompletedCallsByAnimalId(int animalId)
        {
            return new ResponseDto<IEnumerable<CallDto>>
            {
                Data = _callsRepository.GetCompletedCallsByAnimalId(animalId).Select(MapBaseCallToDto)
            };
        }

        [HttpGet("uncompleted")]
        [Authorize(Roles = Constants.VETERINAR_ROLE)]
        public ResponseDto<IEnumerable<CallDto>> GetUncompletedCalls()
        {
            return new ResponseDto<IEnumerable<CallDto>>
            {
                Data = _callsRepository.GetUncompletedCalls().Select(MapBaseCallToDto)
            };
        }

        private static CallDto MapBaseCallToDto(BaseCall call)
        {
            var callsDto = new CallDto
            {
                Address = call.Address,
                BreedName = call.Animal.Breed.Name,
                Date = call.Date,
                Id = call.Id,
                WorkType = call.WorkType.Name
            };

            if (call is CallTreatment callTreatment)
                callsDto.WorkName = callTreatment.Treatment.Name;
            else if (call is CallVaccine callVaccine)
                callsDto.WorkName = callVaccine.Vaccine.Name;

            return callsDto;
        }

        private BaseCall MapCallDtoToCallTreatment(CallDto callDto)
        {
            return new CallTreatment
            {
                Address = callDto.Address,
                Animal = _animalsRepository.GetById(callDto.AnimalId),
                Date = callDto.Date,
                Id = callDto.Id,
                Treatment = _treatmentsRepository.GetById(callDto.WorkId)
            };
        }

        private BaseCall MapCallDtoToCallVaccine(CallDto callVaccine)
        {
            return new CallVaccine
            {
                Address = callVaccine.Address,
                Animal = _animalsRepository.GetById(callVaccine.AnimalId),
                Date = callVaccine.Date,
                Id = callVaccine.Id,
                Vaccine = _vaccinesRepository.GetById(callVaccine.WorkId)
            };
        }
    }
}