﻿namespace Vaccination.WebApi.Controllers
{
    using System.Collections.Generic;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    using Vaccination.DataModel;
    using Vaccination.DataModel.DTO;
    using Vaccination.DataModel.Interfaces;
    using Vaccination.DataModel.Models;

    [Route("api/breeds")]
    [Authorize(Roles = Constants.VETERINAR_ROLE + "," + Constants.USER_ROLE)]
    public class BreedsController : ControllerBase
    {
        private readonly IBreedsRepository _breedsRepository;

        public BreedsController(IBreedsRepository breedsRepository)
        {
            _breedsRepository = breedsRepository;
        }

        [HttpGet("all")]
        public ResponseDto<IEnumerable<Breed>> GetAllBreeds()
        {
            return new ResponseDto<IEnumerable<Breed>>
            {
                Data = _breedsRepository.GetAll()
            };
        }

        [HttpGet("{id}")]
        public ResponseDto<Breed> GetBreedById(int id)
        {
            return new ResponseDto<Breed>
            {
                Data = _breedsRepository.GetById(id)
            };
        }
    }
}