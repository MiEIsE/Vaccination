﻿namespace Vaccination.WebApi.Controllers
{
    using System.Collections.Generic;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    using Vaccination.DataModel;
    using Vaccination.DataModel.DTO;
    using Vaccination.DataModel.Interfaces;
    using Vaccination.DataModel.Models;

    [Route("api/vaccines")]
    [Authorize(Roles = Constants.VETERINAR_ROLE + "," + Constants.USER_ROLE)]
    public class VaccinesController : ControllerBase
    {
        private readonly IVaccinesRepository _vaccinesRepository;

        public VaccinesController(IVaccinesRepository vaccinesRepository)
        {
            _vaccinesRepository = vaccinesRepository;
        }

        [HttpGet("all")]
        public ResponseDto<IEnumerable<Vaccine>> GetAllVaccines()
        {
            return new ResponseDto<IEnumerable<Vaccine>>
            {
                Data = _vaccinesRepository.GetAll()
            };
        }

        [HttpGet("{id}")]
        public ResponseDto<Vaccine> GetVaccineById(int id)
        {
            return new ResponseDto<Vaccine>
            {
                Data = _vaccinesRepository.GetById(id)
            };
        }
    }
}