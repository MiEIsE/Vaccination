﻿namespace Vaccination.WebApi.Controllers
{
    using Microsoft.AspNetCore.Mvc;

    using Vaccination.DataModel.DTO;
    using Vaccination.DataModel.Interfaces;

    [Route("api/auth")]
    public class UsersController : ControllerBase
    {
        private readonly IUsersRepository _usersRepository;

        public UsersController(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        [HttpPut("editUser")]
        public ResponseDto EditUser([FromBody] EditUserProfileDto editUserProfileDto)
        {
            _usersRepository.EditUser(editUserProfileDto);

            return new ResponseDto();
        }

        [HttpPost("register")]
        public ResponseDto Register([FromBody] RegisterDto registerDto)
        {
            _usersRepository.Register(registerDto);

            return new ResponseDto();
        }

        [HttpPost("signIn")]
        public ResponseDto<SignInResultDto> SignIn([FromBody] SignInDto signInDto)
        {
            return new ResponseDto<SignInResultDto>
            {
                Data = _usersRepository.SignIn(signInDto)
            };
        }
    }
}