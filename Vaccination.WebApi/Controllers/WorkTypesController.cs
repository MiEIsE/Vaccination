﻿namespace Vaccination.WebApi.Controllers
{
    using System.Collections.Generic;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    using Vaccination.DataModel;
    using Vaccination.DataModel.DTO;
    using Vaccination.DataModel.Interfaces;
    using Vaccination.DataModel.Models;

    [Route("api/workTypes")]
    [Authorize(Roles = Constants.VETERINAR_ROLE + "," + Constants.USER_ROLE)]
    public class WorkTypesController : ControllerBase
    {
        private readonly IWorkTypesRepository _workTypesRepository;

        public WorkTypesController(IWorkTypesRepository workTypesRepository)
        {
            _workTypesRepository = workTypesRepository;
        }

        [HttpGet("all")]
        public ResponseDto<IEnumerable<WorkType>> GetAllWorkTypes()
        {
            return new ResponseDto<IEnumerable<WorkType>>
            {
                Data = _workTypesRepository.GetAll()
            };
        }

        [HttpGet("{id}")]
        public ResponseDto<WorkType> GetWorkTypeById(int id)
        {
            return new ResponseDto<WorkType>
            {
                Data = _workTypesRepository.GetById(id)
            };
        }
    }
}