﻿namespace Vaccination.WebApi.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    using Vaccination.DataModel.DTO;
    using Vaccination.DataModel.Interfaces;
    using Vaccination.DataModel.Models;
    using Vaccination.DataModel;

    [Route("api/animals")]
    public class AnimalsController : ControllerBase
    {
        private readonly IAnimalsRepository _animalsRepository;

        private readonly IBreedsRepository _breedsRepository;

        private readonly IUsersRepository _usersRepository;

        public AnimalsController(IAnimalsRepository animalsRepository,
            IBreedsRepository breedsRepository, IUsersRepository usersRepository)
        {
            _animalsRepository = animalsRepository;
            _breedsRepository = breedsRepository;
            _usersRepository = usersRepository;
        }

        [HttpPost("create")]
        [Authorize(Roles = Constants.USER_ROLE)]
        public ResponseDto CreateAnimal([FromBody] AnimalDto animalDto)
        {
            _animalsRepository.Create(MapAnimalsDtoToModel(animalDto));
            return new ResponseDto();
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = Constants.USER_ROLE)]
        public ResponseDto DeleteAnimal(int id)
        {
            _animalsRepository.Delete(id);
            return new ResponseDto();
        }

        [HttpPut("edit")]
        [Authorize(Roles = Constants.USER_ROLE)]
        public ResponseDto EditAnimal([FromBody] AnimalDto animalDto)
        {
            _animalsRepository.Edit(MapAnimalsDtoToModel(animalDto));
            return new ResponseDto();
        }

        [HttpGet("all")]
        [Authorize(Roles = Constants.VETERINAR_ROLE + "," + Constants.USER_ROLE)]
        public ResponseDto<IEnumerable<AnimalDto>> GetAllAnimals()
        {
            return new ResponseDto<IEnumerable<AnimalDto>>
            {
                Data = _animalsRepository.GetAll().Select(MapAnimalsModelToDto)
            };
        }

        [HttpGet("{id}")]
        [Authorize(Roles = Constants.VETERINAR_ROLE + "," + Constants.USER_ROLE)]
        public ResponseDto<AnimalDto> GetAnimalById(int id)
        {
            return new ResponseDto<AnimalDto>
            {
                Data = MapAnimalsModelToDto(_animalsRepository.GetById(id))
            };
        }

        [HttpGet("byUserId/{id}")]
        [Authorize(Roles = Constants.USER_ROLE)]
        public ResponseDto<IEnumerable<AnimalDto>> GetAnimalsByUserId(string id)
        {
            return new ResponseDto<IEnumerable<AnimalDto>>
            {
                Data = _animalsRepository.GetAnimalsByUserId(id).Select(MapAnimalsModelToDto)
            };
        }

        private Animal MapAnimalsDtoToModel(AnimalDto animalDto)
        {
            return new Animal
            {
                Birthday = animalDto.Birthday,
                Breed = _breedsRepository.GetById(animalDto.BreedId),
                Id = animalDto.Id,
                Name = animalDto.Name,
                User = _usersRepository.GetUserById(animalDto.UserId)
            };
        }

        private static AnimalDto MapAnimalsModelToDto(Animal animal)
        {
            return new AnimalDto
            {
                Birthday = animal.Birthday,
                BreedId = animal.Breed?.Id ?? 0,
                BreedName = animal.Breed?.Name ?? string.Empty,
                Id = animal.Id,
                Name = animal.Name,
                UserId = animal.User?.Id ?? string.Empty
            };
        }
    }
}