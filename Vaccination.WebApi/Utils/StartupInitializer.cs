﻿namespace Vaccination.WebApi.Utils
{
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Identity;

    using Vaccination.DataModel;

    public static class StartupInitializer
    {
        public static async Task Initialize(UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            await InitRoles(roleManager);
            await CreateDefaultUser(userManager);
        }

        private static async Task CreateDefaultUser(UserManager<IdentityUser> userManager)
        {
            var veterinars = userManager.GetUsersInRoleAsync(Constants.VETERINAR_ROLE).Result;

            if (veterinars.Any())
                return;

            var veterinar = new IdentityUser
            {
                UserName = "veterinar"
            };

            await userManager.CreateAsync(veterinar, "QWEqwe123!");
            await userManager.AddToRolesAsync(veterinar, new[] { Constants.VETERINAR_ROLE });
        }

        private static async Task EnsureRoleAsync(RoleManager<IdentityRole> roleManager, string roleName)
        {
            var role = roleManager.Roles
                .FirstOrDefault(x => x.NormalizedName == roleName);

            if (role != null)
                return;

            role = new IdentityRole(roleName);
            await roleManager.CreateAsync(role);
        }

        private static async Task InitRoles(RoleManager<IdentityRole> roleManager)
        {
            await EnsureRoleAsync(roleManager, Constants.USER_ROLE);
            await EnsureRoleAsync(roleManager, Constants.VETERINAR_ROLE);
        }
    }
}