﻿function showGraphics() {
    hideAllViews();
    $("#graphicsView").show();
    $.ajax({
        url: "/api/graphics/all",
        type: "GET",
        contentType: "application/json",
        headers: getAuthHeader(),
        success: function (message) {
            var graphics = message.data;
            var rows = "";
            $("#graphicsTable tbody").empty();
            $.each(graphics, function (index, graphic) {
                rows += graphicsRow(graphic);
            });
            $("#graphicsTable").append(rows);
        },
        error: function (message) {
            alert(message.responseJSON.Data);
        }
    });
}

function graphicsRow(graphic) {
	return "<tr data-rowid='" + graphic.id + "'>" +
		"<td>" + graphic.breedName + "</td>" +
		"<td>" + graphic.vaccineName + "</td>" +
		"<td>" + graphic.term + "</td>" +
		"</tr>";
}

function showNotifications() {
    hideAllViews();
    $("#notificationsView").show();
    $.ajax({
        url: "/api/graphics/notifications/" + userId,
        type: "GET",
        contentType: "application/json",
        headers: getAuthHeader(),
        success: function (message) {
            var notifications = message.data;
            var rows = "";
            $("#notificationsTable tbody").empty();
            $.each(notifications, function (index, notification) {
                rows += notificationsRow(notification);
            });
            $("#notificationsTable").append(rows);
        },
        error: function (message) {
            alert(message.responseJSON.Data);
        }
    });
}

function notificationsRow(notification) {
	return "<tr>" +
		"<td>" + notification.animalName + "</td>" +
		"<td>" + notification.breedName + "</td>" +
		"<td>" + notification.vaccineName + "</td>" +
		"<td>" + moment(notification.vaccineDate).format('DD.MM.YYYY') + "</td>" +
		"</tr>";
}