﻿//заполнение выпадающего списка вакцин
function getVaccineByBreedId(breedId) {
    $.ajax({
        url: "/api/graphics/breedId/" + breedId,
        type: "GET",
        contentType: "application/json",
        headers: getAuthHeader(),
        success: function (message) {
            var graphics = message.data;
            $("#vaccineSelect").empty();
            var rows = "";
            $.each(graphics, function (index, graphic) {
                rows += "<option value=" + graphic.vaccineId + ">" + graphic.vaccineName + "</option>";
            });
            $("#vaccineSelect").append(rows);
        },
        error: function (message) {
            alert(message.responseJSON.Data);
        }
    });
}

//заполнение выпадающего списка лечений
function getAllTreatments() {
    $.ajax({
        url: "/api/treatments/all",
        type: "GET",
        contentType: "application/json",
        headers: getAuthHeader(),
        success: function (message) {
            var treatments = message.data;
            $("#treatmentSelect").empty();
            var rows = "";
            $.each(treatments, function (index, treatment) {
                rows += "<option value=" + treatment.id + ">" + treatment.name + "</option>";
            });
            $("#treatmentSelect").append(rows);
        },
        error: function (message) {
            alert(message.responseJSON.Data);
        }
    });
}

//показ формы вызова на вакцину
function showCallVaccineView() {
	hideAllViews();
	$("#address").val("");
    getAnimalsByUserId();
    showVaccinesByAnimal();
    $("#callVaccineView").show();
    var today = moment(moment()).format('YYYY-MM-DD');
    $("#date").val(today);
}

function showVaccinesByAnimal() {
    var id = $("#animalSelect option:selected").val();
    getAnimalByIdVaccineCall(id);
}

function getAnimalByIdVaccineCall(id) {
    $.ajax({
        url: "/api/animals/" + id,
        type: "GET",
        contentType: "application/json",
        headers: getAuthHeader(),
        success: function (message) {
            var selectedAnimal = message.data;
            getVaccineByBreedId(selectedAnimal.breedId);
            $("#breedVaccineId").text(selectedAnimal.breedId);
            $("#breedVaccineName").text(selectedAnimal.breedName);
        },
        error: function (message) {
            alert(message.responseJSON.Data);
        }
    });
}

function createCallVaccine() {
    var today = moment(moment()).format('YYYY-MM-DD');
    if ($("#date").val() < today) {
        alert("Дата лечения меньше текущей даты.");
    }
    else {
        $.ajax({
            url: "/api/calls/createVaccine",
            method: "POST",
            contentType: "application/json",
            headers: getAuthHeader(),
            data: JSON.stringify({
                address: $("#address").val(),
                animalId: $("#animalSelect option:selected").val(),
                breedName: $("#breedVaccineName").text(),
                date: $("#date").val(),
                id: 0,
                workId: $("#vaccineSelect option:selected").val(),
                workName: $("#vaccineSelect option:selected").text()
            }),
            success: function () {
                getAllAnimals();
                showAnimals();
            },
            error: function (message) {
                alert(message.responseJSON.Data);
            }
        });
    }
}

function showCallTreatmentView() {
	hideAllViews();
	$("#treatmentAddress").val("");
    $.ajax({
        url: "/api/animals/byUserId/" + userId,
        type: "GET",
        contentType: "application/json",
        headers: getAuthHeader(),
        success: function (message) {
            var animals = message.data;
            $("#animalTreatmentSelect").empty();
            var rows = "";
            $.each(animals,
                function (index, animal) {
                    rows += "<option value=" + animal.id + ">" + animal.name + "</option>";
                });
            $("#animalTreatmentSelect").append(rows);

            getBreedFromSelectedAnimalTreatment();
            $("#callTreatmentView").show();
            var today = moment(moment()).format('YYYY-MM-DD');
            $("#treatmentDate").val(today);
        },
        error: function (message) {
            alert(message.responseJSON.Data);
        }
    });
}

function getBreedFromSelectedAnimalTreatment() {
    var animalId = $("#animalTreatmentSelect option:selected").val();
    $.ajax({
        url: "/api/animals/" + animalId,
        type: "GET",
        contentType: "application/json",
        headers: getAuthHeader(),
        success: function (message) {
            var selectedAnimal = message.data;
            $("#breedTreatmentId").text(selectedAnimal.breedId);
            $("#breedTreatmentName").text(selectedAnimal.breedName);
        },
        error: function (message) {
            alert(message.responseJSON.Data);
        }
    });
}
//добавить вызов на лечение
function createCallTreatment() {
    var today = moment(moment()).format('YYYY-MM-DD');
    if ($("#treatmentDate").val() < today) {
        alert("Дата лечения меньше текущей даты.");
    }
    else {
        $.ajax({
            url: "/api/calls/createTreatment",
            method: "POST",
            contentType: "application/json",
            headers: getAuthHeader(),
            data: JSON.stringify({
                address: $("#treatmentAddress").val(),
                animalId: $("#animalTreatmentSelect option:selected").val(),
                breedName: $("#breedTreatmentName").text(),
                date: $("#treatmentDate").val(),
                id: 0,
                workId: $("#treatmentSelect option:selected").val(),
                workName: $("#treatmentSelect option:selected").text()
            }),
            success: function () {
                getAllAnimals();
                showAnimals();
            },
            error: function (message) {
                alert(message.responseJSON.Data);
            }
        });
    }
}
//показать форму вызовов
function showCalls() {
    getAllCalls();
    hideAllViews();
    $("#callsView").show();
}
//получение всех незавершенных вызовов
function getAllCalls() {
    $.ajax({
        url: "/api/calls/uncompleted",
        type: "GET",
        contentType: "application/json",
        headers: getAuthHeader(),
        success: function (message) {
            var calls = message.data;
            var rows = "";
            $("#callsTable tbody").empty();
            $.each(calls,
                function (index, call) {
                    rows += callRow(call);
                });
            $("#callsTable").append(rows);
        },
        error: function (message) {
            alert(message.responseJSON.Data);
        }
    });
}
//заполнение таблицы вызовов
function callRow(call) {
    return "<tr data-rowid='" + call.id + "'>" +
        "<td>" + call.workType + "</td>" +
        "<td>" + call.workName + "</td>" +
        "<td>" + moment(call.date).format('DD.MM.YYYY') + "</td>" +
        "<td>" + call.address + "</td>" +
        "<td>" + call.breedName + "</td>" +
        "<td><a onclick=completeCall(" + call.id + ")>Завершить</a></td>" +
        "</tr>";
}
//завершение вызова
function completeCall(id) {
    $.ajax({
        url: "/api/calls/complete/" + id,
        type: "PUT",
        contentType: "application/json",
        headers: getAuthHeader(),
        success: function () {
            showCalls();
        },
        error: function (message) {
            alert(message.responseJSON.Data);
        }
    });
}