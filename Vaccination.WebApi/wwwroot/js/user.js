﻿var token;
var userId;
var userName;
var role;
var veterinarRole = "veterinar";
var userRole = "user";

function signIn() {
	$.ajax({
		url: "/api/auth/signIn",
		method: "POST",
        contentType: "application/json",
		data: JSON.stringify({
			userName: $("#login").val(),
			password: $("#password").val()
		}),
		success: function(signInResultDto) {
			var data = signInResultDto.data;
			token = data.token;
			userId = data.id;
			userName = $("#login").val();
			role = data.role;

			showAuthorizedTabs();
			showAnimals();
            getAllAnimals();
            getAllBreeds();
            getAnimalsByUserId();
		    getAllTreatments();
		},
		error: function(message) {
			alert(message.responseJSON.Data);
		}
	});
}

function signOut() {
	token = null;
	userId = null;
	role = null;
	showUnauthorzedTabs();
	showSignInView();
}

function register() {
	$.ajax({
		url: "/api/auth/register",
		method: "POST",
		contentType: "application/json",
		data: JSON.stringify({
			userName: $("#registerLogin").val(),
			password: $("#registerPassword").val(),
			confirmPassword: $("#registerConfirmPassword").val()
		}),
		success: function() {
			showSignInView();
		},
		error: function(message) {
			alert(message.responseJSON.Data);
		}
	});
}

function updateProfile() {
	$.ajax({
		url: "/api/auth/editUser",
		method: "PUT",
        contentType: "application/json",
		headers: getAuthHeader(),
		data: JSON.stringify({
			userName: $("#profileLogin").val(),
			userId: userId,
			password: $("#profilePassword").val(),
			confirmPassword: $("#profileConfirmPassword").val()
		}),
		success: function () {
			showAnimals();
		},
		error: function (message) {
			alert(message.responseJSON.Data);
		}
	});
}

function showRegisterView() {
	hideAllViews();
	$("#registerLogin").val("");
	$("#registerPassword").val("");
	$("#registerConfirmPassword").val("");
	$("#registerView").show();
}

function showSignInView() {
	hideAllViews();
	$("#login").val("user1");
	$("#password").val("QWEqwe123!");
	$("#signInView").show();
}

function showProfileView() {
	hideAllViews();
	$("#profileLogin").val(userName);
	$("#profilePassword").val("");
	$("#profileConfirmPassword").val("");
	$("#profileView").show();
}

function showAuthorizedTabs() {
	hideAllTabs();
	$("#mainTab").show();
    $("#graphicsTab").show();
    $("#signOutTab").show();

    if (role == userRole) {
        $("#callVaccineTab").show();
        $("#callTreatmentTab").show();
        $("#notificationsTab").show();
        $("#profileTab").show();
    }
    else {
        $("#callsTab").show();
    }
}

function showUnauthorzedTabs() {
	hideAllTabs();
	$("#signInTab").show();
	$("#registerTab").show();
}