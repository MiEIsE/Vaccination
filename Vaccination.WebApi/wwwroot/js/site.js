﻿function hideAllViews() {
	var divs = document.getElementsByTagName("div");
	for (var i = 0; i < divs.length; i++) {
		divs[i].style.display = "none";
	}

	$("#navigationBar").show();
}

function hideAllTabs() {
	var lis = document.getElementsByTagName("li");
	for (var i = 0; i < lis.length; i++) {
		lis[i].style.display = "none";
	}
}

$(document).ready(function () {
    showUnauthorzedTabs();
    showSignInView();
});

function today() {
	return moment().format('YYYY-MM-DD');
}

function getAuthHeader() {
    return { "Authorization": "Bearer " + token };
}