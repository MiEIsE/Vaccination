﻿function getAllAnimals() {
    if (role == userRole) {
        $.ajax({
            url: "/api/animals/byUserId/" + userId,
            type: "GET",
            contentType: "application/json",
            headers: getAuthHeader(),
            success: function(message) {
                var animals = message.data;
                var rows = "";
                $("#animalsTable tbody").empty();
                $.each(animals,
                    function(index, animal) {
                        rows += rowUser(animal);
                    });
                $("#animalsTable").append(rows);
            },
            error: function(message) {
                alert(message.responseJSON.Data);
            }
        });
    } else {
        $.ajax({
            url: "/api/animals/all",
            type: "GET",
            contentType: "application/json",
            headers: getAuthHeader(),
            success: function (message) {
                var animals = message.data;
                var rows = "";
                $("#animalsTable tbody").empty();
                $.each(animals,
                    function (index, animal) {
                        rows += rowVeterinar(animal);
                    });
                $("#animalsTable").append(rows);
            },
            error: function (message) {
                alert(message.responseJSON.Data);
            }
        });
    }
}

function editAnimalById(id) {
	$.ajax({
		url: "/api/animals/" + id,
		method: "GET",
		contentType: "application/json",
		headers: getAuthHeader(),
		success: function (message) {
			var animal = message.data;
			showEditAnimalView(animal);
		},
		error: function () {
			alert("При получении животного возникла ошибка.");
		}
	});
}

function getAnimalsByUserId() {
    $.ajax({
        url: "/api/animals/byUserId/" + userId,
        type: "GET",
        contentType: "application/json",
        headers: getAuthHeader(),
        success: function(message) {
            var animals = message.data;
            $("#animalSelect").empty();
            var rows = "";
            $.each(animals,
                function(index, animal) {
                    rows += "<option value=" + animal.id + ">" + animal.name + "</option>";
                });
            $("#animalSelect").append(rows);
        },
        error: function() {
            alert(message.responseJSON.Data);
        }
    });
}
//заполнение пород при добавлении и редактировании животного
function getAllBreeds() {
    $.ajax({
        url: "/api/breeds/all",
        type: "GET",
        contentType: "application/json",
        headers: getAuthHeader(),
        success: function (message) {
            var breeds = message.data;
            $("#breedSelect").empty();
            var rows = "";
            $.each(breeds, function (index, breed) {
                rows += "<option value=" + breed.id + ">" + breed.name + "</option>";
            });
            $("#breedSelect").append(rows);
        },
        error: function () {
            alert(message.responseJSON.Data);
        }
    });
}

function rowVeterinar(animal) {
    return "<tr data-rowid='" + animal.id + "'>" +
        "<td>" + animal.name + "</td>" +
        "<td>" + animal.breedName + "</td>" +
        "<td>" + moment(animal.birthday).format('DD.MM.YYYY') + "</td>" +
        "<td><a onclick=viewVaccinationCard(" + animal.id + ")>Карта вакцинации</a></td>" +
        "</tr>";
}

function rowUser(animal) {
    return "<tr data-rowid='" + animal.id + "'>" +
        "<td>" + animal.name + "</td>" +
        "<td>" + animal.breedName + "</td>" +
        "<td>" + moment(animal.birthday).format('DD.MM.YYYY') + "</td>" +
        "<td><a onclick=editAnimalById(" + animal.id + ")>Редактировать</a></td>" +
        "<td><a onclick=deleteAnimal(" + animal.id + ")>Удалить</a></td>" +
        "<td><a onclick=viewVaccinationCard(" + animal.id + ")>Карта вакцинации</a></td>" +
        "</tr>";
}

function showAnimals() {
	hideAllViews();
    $("#mainView").show();

    if (role == userRole) {
        $("#buttonCreate").show();
    } else {
        $("#buttonCreate").hide();
    }
}

function showEditAnimalView(animal) {
    $("#editAnimalViewHeader").show();
    $("#createAnimalViewHeader").hide();
    $("#id").val(animal.id);
    $("#name").val(animal.name);

    var date = moment(animal.birthday).format('YYYY-MM-DD');
    $("#birthday").val(date);

    $("#mainView").hide();
    $("#createAnimalView").show();
}

function createOrEditAnimal() {
    if ($("#id").val().length <= 0) {
        createAnimal();
    } else {
        editAnimal();
    }
}

function editAnimal() {
	var animalToEdit = {
		id: $("#id").val(),
		name: $("#name").val(),
		breedId: $("#breedSelect option:selected").val(),
		breedName: $("#breedSelect option:selected").text(),
		birthday: $("#birthday").val(),
		userId: userId
	};
	var animalToEditJson = JSON.stringify(animalToEdit);
    $.ajax({
        url: "/api/animals/edit",
        method: "PUT",
        contentType: "application/json",
        headers: getAuthHeader(),
		data: animalToEditJson,
        success: function () {
            $("#animalsTable tr[data-rowid=" + animalToEdit.id + "]").replaceWith(rowUser(animalToEdit));
            showAnimals();
        },
        error: function (message) {
            alert(message.responseJSON.Data);
        }
    });
}

function showCreateAnimalView()
{
    $("#id").val('');
    $("#name").val('');
    var date = moment(moment()).format('YYYY-MM-DD');
    $("#birthday").val(date);
    hideAllViews();
    $("#createAnimalView").show();
    $("#editAnimalViewHeader").hide();
    $("#createAnimalViewHeader").show();

}

function createAnimal() {
    $.ajax({
        url: "/api/animals/create",
        method: "POST",
        contentType: "application/json",
        headers: getAuthHeader(),
        data: JSON.stringify({
            id: 0,
            name: $("#name").val(),
            breedId: $("#breedSelect option:selected").val(),
            breedName: $("#breedSelect option:selected").text(),
            birthday: $("#birthday").val(),
            userId: userId
        }),
        success: function () {
            getAllAnimals();
            showAnimals();
        },
        error: function (message) {
	        alert(message.responseJSON.Data);
        }
    });
}

function deleteAnimal(id) {
    $.ajax({
        url: "/api/animals/" + id,
        method: "DELETE",
        contentType: "application/json",
        headers: getAuthHeader(),
        success: function () {
            getAllAnimals();
            showAnimals();
        },
		error: function (message) {
			alert(message.responseJSON.Data);
		}
    });
}

//показ карты вакцинации
function viewVaccinationCard(id) {
    hideAllViews();
    getAnimalByIdVaccinationCard(id);
    $("#vaccinationCardView").show();
    $.ajax({
        url: "/api/calls/completed/" + id,
        method: "GET",
        contentType: "application/json",
        headers: getAuthHeader(),
        success: function (message) {
            var calls = message.data;
            var rows = "";
            $("#vaccinationCardTable tbody").empty();
            $.each(calls,
                function (index, call) {
                    rows += cardRow(call);
                });
            $("#vaccinationCardTable").append(rows);
        },
        error: function (message) {
            alert(message.responseJSON.Data);
        }
    });
}
//заполнение таблицы карты вакцинации
function cardRow(call) {
    return "<tr data-rowid='" + call.id + "'>" +
        "<td>" + call.workType + "</td>" +
        "<td>" + call.workName + "</td>" +
        "<td>" + moment(call.date).format('DD.MM.YYYY') + "</td>" +
        "</tr>";
}
//заполнение клички в форме карты вакцинации
function getAnimalByIdVaccinationCard(id) {
    $.ajax({
        url: "/api/animals/" + id,
        type: "GET",
        contentType: "application/json",
        headers: getAuthHeader(),
        success: function (message) {
            var animal = message.data;
            $("#animalNameVaccinationCard").text(animal.name);
        },
        error: function (message) {
            alert(message.responseJSON.Data);
        }
    });
}