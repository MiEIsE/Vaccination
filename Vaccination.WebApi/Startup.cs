﻿namespace Vaccination.WebApi
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.HttpOverrides;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.IdentityModel.Tokens;

    using Swashbuckle.AspNetCore.Swagger;

    using Vaccination.DataModel;
    using Vaccination.DataModel.Interfaces;
    using Vaccination.EFRepository;
    using Vaccination.EFRepository.Repositories;
    using Vaccination.WebApi.Middlewares;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseDeveloperExceptionPage();

            app.UseHttpStatusCodeExceptionMiddleware();

            app.UseAuthentication();

            app.UseCors("AllowAll");

            app.UseMvc();

            app.UseForwardedHeaders(
                new ForwardedHeadersOptions
                {
                    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
                });

            app.UseSwagger();

            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1"));

            app.UseDefaultFiles();

            app.UseStaticFiles();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<VaccinationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddCors(
                options => options.AddPolicy(
                    "AllowAll",
                    builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader().AllowCredentials()));

            services.AddMvc();

            services.AddDistributedMemoryCache();

            services.AddIdentity<IdentityUser, IdentityRole>(options =>
                {
                    options.Password.RequireDigit = true;
                    options.Password.RequiredLength = 6;
                    options.Password.RequireLowercase = true;
                    options.Password.RequireUppercase = true;
                    options.Password.RequireNonAlphanumeric = false;
                })
                .AddEntityFrameworkStores<VaccinationDbContext>()
                .AddDefaultTokenProviders();

            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        IssuerSigningKey = Constants.AuthOptions.GetSymmetricSecurityKey(),
                        ValidateIssuerSigningKey = true
                    };
                    options.SaveToken = true;
                });

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
                options.AddSecurityDefinition(
                    "Bearer",
                    new ApiKeyScheme
                    {
                        Description = "Please enter JWT with Bearer into field",
                        Name = "Authorization",
                        In = "header",
                        Type = "apiKey"
                    });
                options.AddSecurityRequirement(
                    new Dictionary<string, IEnumerable<string>> { { "Bearer", Enumerable.Empty<string>() } });
            });

            services.AddScoped<IAnimalsRepository, AnimalsRepository>();
            services.AddScoped<IUsersRepository, UsersRepository>();
            services.AddScoped<IBreedsRepository, BreedsRepository>();
            services.AddScoped<IVaccinesRepository, VaccinesRepository>();
            services.AddScoped<ITreatmentsRepository, TreatmentRepository>();
            services.AddScoped<IWorkTypesRepository, WorkTypesRepository>();
            services.AddScoped<IGraphicsRepository, GraphicsRepository>();
            services.AddScoped<ICallsRepository, CallsRepository>();
        }
    }
}