﻿namespace Vaccination.DataModel
{
    using System.Text;

    using Microsoft.IdentityModel.Tokens;

    public class Constants
    {
        public const int TREATMENT_WORKTYPE = 1;

        public const string USER_ROLE = "user";

        public const int VACCINE_WORKTYPE = 2;

        public const string VETERINAR_ROLE = "veterinar";

        public class AuthOptions
        {
            private const string KEY = "mysupersecret_secretkey!123";

            public const int LIFETIMEDAYS = 365;

            public static SymmetricSecurityKey GetSymmetricSecurityKey()
            {
                return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
            }
        }
    }
}