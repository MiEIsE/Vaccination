﻿namespace Vaccination.DataModel.Models
{
    public class CallTreatment : BaseCall
    {
        public Treatment Treatment { get; set; }
    }
}