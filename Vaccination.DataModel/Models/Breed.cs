﻿namespace Vaccination.DataModel.Models
{
    using System.ComponentModel.DataAnnotations;

    public class Breed : BaseIdEntity
    {
        [Required]
        public string Name { get; set; }
    }
}