﻿namespace Vaccination.DataModel.Models
{
    using System.ComponentModel.DataAnnotations;

    public class Graphic : BaseIdEntity
    {
        [Required]
        public Breed Breed { get; set; }

        /// <summary>
        /// Срок проведения вакцины.
        /// </summary>
        [Required]
        public int Term { get; set; }

        [Required]
        public Vaccine Vaccine { get; set; }
    }
}