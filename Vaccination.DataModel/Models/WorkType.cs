﻿namespace Vaccination.DataModel.Models
{
    using System.ComponentModel.DataAnnotations;

    public class WorkType : BaseIdEntity
    {
        [Required]
        public string Name { get; set; }
    }
}