﻿namespace Vaccination.DataModel.Models
{
    using System.ComponentModel.DataAnnotations;

    public class Treatment : BaseIdEntity
    {
        [Required]
        public string Name { get; set; }
    }
}