﻿namespace Vaccination.DataModel.Models
{
    using System.ComponentModel.DataAnnotations;

    public class Vaccine : BaseIdEntity
    {
        [Required]
        public string Name { get; set; }
    }
}