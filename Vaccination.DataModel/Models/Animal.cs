﻿namespace Vaccination.DataModel.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    using Microsoft.AspNetCore.Identity;

    public class Animal : BaseIdEntity
    {
        [Required]
        public DateTime Birthday { get; set; }

        [Required]
        public Breed Breed { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public IdentityUser User { get; set; }
    }
}