﻿namespace Vaccination.DataModel.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class BaseCall : BaseIdEntity
    {
        [Required]
        public string Address { get; set; }

        [Required]
        public Animal Animal { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public bool IsComplete { get; set; }

        [Required]
        public WorkType WorkType { get; set; }
    }
}