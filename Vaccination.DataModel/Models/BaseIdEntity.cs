﻿namespace Vaccination.DataModel.Models
{
    public class BaseIdEntity
    {
        public int Id { get; set; }
    }
}