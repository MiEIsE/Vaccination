﻿namespace Vaccination.DataModel.Models
{
    public class CallVaccine : BaseCall
    {
        public Vaccine Vaccine { get; set; }
    }
}