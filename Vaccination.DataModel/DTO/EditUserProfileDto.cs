﻿namespace Vaccination.DataModel.DTO
{
    public class EditUserProfileDto
    {
        public string ConfirmPassword { get; set; }

        public string Password { get; set; }

        public string UserId { get; set; }

        public string UserName { get; set; }
    }
}