﻿namespace Vaccination.DataModel.DTO
{
    using System.Net;

    public class ResponseDto
    {
        public ResponseDto()
        {
            Status = ((int) HttpStatusCode.OK).ToString();
            Data = "OK";
        }

        public string Data { get; set; }

        public string Status { get; set; }
    }

    public class ResponseDto<T> : ResponseDto
    {
        public new T Data { get; set; }
    }
}