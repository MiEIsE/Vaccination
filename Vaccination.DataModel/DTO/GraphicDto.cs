﻿namespace Vaccination.DataModel.DTO
{
    public class GraphicDto
    {
        public int BreedId { get; set; }

        public string BreedName { get; set; }

        public int Id { get; set; }

        /// <summary>
        /// Срок проведения вакцины.
        /// </summary>
        public int Term { get; set; }

        public int VaccineId { get; set; }

        public string VaccineName { get; set; }
    }
}