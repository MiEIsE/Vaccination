﻿namespace Vaccination.DataModel.DTO
{
    public class SignInResultDto
    {
        public string Id { get; set; }

        public string Role { get; set; }
        public string Token { get; set; }
    }
}