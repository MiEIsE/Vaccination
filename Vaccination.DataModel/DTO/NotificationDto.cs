﻿namespace Vaccination.DataModel.DTO
{
    using System;

    public class NotificationDto
    {
        public string AnimalName { get; set; }

        public string BreedName { get; set; }

        public DateTime VaccineDate { get; set; }

        public string VaccineName { get; set; }
    }
}