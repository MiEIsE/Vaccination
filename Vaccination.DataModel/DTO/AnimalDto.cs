﻿namespace Vaccination.DataModel.DTO
{
    using System;

    public class AnimalDto
    {
        public DateTime Birthday { get; set; }

        public int BreedId { get; set; }

        public string BreedName { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public string UserId { get; set; }
    }
}