﻿namespace Vaccination.DataModel.DTO
{
    using System;

    public class CallDto
    {
        public string Address { get; set; }

        public int AnimalId { get; set; }

        public string BreedName { get; set; }

        public DateTime Date { get; set; }

        public int Id { get; set; }

        public int WorkId { get; set; }

        public string WorkName { get; set; }

        public string WorkType { get; set; }
    }
}