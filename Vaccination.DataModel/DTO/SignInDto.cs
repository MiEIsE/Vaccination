﻿namespace Vaccination.DataModel.DTO
{
    public class SignInDto
    {
        public string Password { get; set; }

        public string UserName { get; set; }
    }
}