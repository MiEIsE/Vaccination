﻿namespace Vaccination.DataModel.DTO
{
    public class RegisterDto
    {
        public string ConfirmPassword { get; set; }

        public string Password { get; set; }

        public string UserName { get; set; }
    }
}