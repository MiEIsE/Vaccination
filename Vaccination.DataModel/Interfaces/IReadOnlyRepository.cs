﻿namespace Vaccination.DataModel.Interfaces
{
    using System.Collections.Generic;

    public interface IReadOnlyRepository<T>
    {
        IEnumerable<T> GetAll();

        T GetById(int id);

        void Validate(T data);
    }
}