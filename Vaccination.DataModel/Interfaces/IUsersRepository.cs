﻿namespace Vaccination.DataModel.Interfaces
{
    using Microsoft.AspNetCore.Identity;

    using Vaccination.DataModel.DTO;

    public interface IUsersRepository
    {
        void EditUser(EditUserProfileDto editUserProfileDto);

        IdentityUser GetUserById(string id);

        void Register(RegisterDto registerDto);

        SignInResultDto SignIn(SignInDto signInDto);

        void Validate(IdentityUser user);
    }
}