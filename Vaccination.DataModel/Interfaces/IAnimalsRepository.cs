﻿namespace Vaccination.DataModel.Interfaces
{
    using System.Collections.Generic;

    using Vaccination.DataModel.Models;

    public interface IAnimalsRepository : ICrudRepository<Animal>
    {
        IEnumerable<Animal> GetAnimalsByUserId(string id);
    }
}