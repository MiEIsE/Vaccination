﻿namespace Vaccination.DataModel.Interfaces
{
    using Vaccination.DataModel.Models;

    public interface ITreatmentsRepository : IReadOnlyRepository<Treatment>
    {
    }
}