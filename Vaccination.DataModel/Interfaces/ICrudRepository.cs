﻿namespace Vaccination.DataModel.Interfaces
{
    public interface ICrudRepository<T> : IReadOnlyRepository<T>
    {
        void Create(T repositoryObject);

        void Delete(int id);

        void Edit(T repositoryObject);
    }
}