﻿namespace Vaccination.DataModel.Interfaces
{
    using System.Collections.Generic;

    using Vaccination.DataModel.Models;

    public interface ICallsRepository
    {
        void CompleteCall(int id);

        void CreateCall(BaseCall call);

        IEnumerable<BaseCall> GetCompletedCallsByAnimalId(int animalId);

        IEnumerable<BaseCall> GetUncompletedCalls();

        void Validate(BaseCall call);
    }
}