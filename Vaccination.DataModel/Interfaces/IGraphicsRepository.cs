﻿namespace Vaccination.DataModel.Interfaces
{
    using System.Collections.Generic;

    using Vaccination.DataModel.Models;

    public interface IGraphicsRepository : IReadOnlyRepository<Graphic>
    {
        IEnumerable<Graphic> GetByBreedId(int breedId);
        IEnumerable<Graphic> GetVaccineByBreedId(int breedId);
    }
}