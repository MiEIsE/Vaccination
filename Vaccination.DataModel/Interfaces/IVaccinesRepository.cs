﻿namespace Vaccination.DataModel.Interfaces
{
    using Vaccination.DataModel.Models;

    public interface IVaccinesRepository : IReadOnlyRepository<Vaccine>
    {
    }
}