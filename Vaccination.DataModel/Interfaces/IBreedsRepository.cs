﻿namespace Vaccination.DataModel.Interfaces
{
    using Vaccination.DataModel.Models;

    public interface IBreedsRepository : IReadOnlyRepository<Breed>
    {
    }
}