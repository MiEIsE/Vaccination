﻿namespace Vaccination.DataModel.Interfaces
{
    using Vaccination.DataModel.Models;

    public interface IWorkTypesRepository : IReadOnlyRepository<WorkType>
    {
    }
}